# pull official base image
FROM python:3.9-alpine

# set work directory
WORKDIR /code

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 1

# install psycopg2
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev \
    && apk add --no-cache gcc libffi-dev g++ build-base linux-headers postgresql-contrib pcre-dev bash alpine-sdk jpeg-dev zlib-dev \
    && apk del build-deps

RUN mkdir logs 

# install dependencies
RUN pip install --upgrade pip
COPY requirements/dev.txt /code/
COPY requirements/common.txt /code/
RUN pip install -r dev.txt

# copy project
COPY . /code/
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
COPY logs /logs

# collect static files
RUN python manage.py collectstatic --noinput

# add and run as non-root user
RUN adduser -D admin
USER admin

# run gunicorn
CMD gunicorn core.config.wsgi:application --bind 0.0.0.0:$PORT
