from rest_framework import serializers

from modules.core.models.city import City
from modules.events.models import ColorGradient


class ColorGradientSerializer(serializers.ModelSerializer):
    class Meta:
        model = ColorGradient
        fields = [
            'id',
            'start_color',
            'end_color',
            'angle'
        ]


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "name",
            "latitude",
            "longitude"
        ]
