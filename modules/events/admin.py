from django.contrib import admin

from modules.events.models import Events, Category, ColorGradient


@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'address', 'color', 'coordinate_x', 'coordinate_y', 'accept_automatic',
                    'price', 'max_count_users', 'create_event', 'search_gender', 'date_event', 'date_create_event']
    search_fields = ['name', ]
    raw_id_fields = ['create_event', ]
    list_filter = ['is_active', "is_online"]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', ]
    search_fields = ['name', ]


@admin.register(ColorGradient)
class ColorGradientAdmin(admin.ModelAdmin):
    list_display = ['id', 'start_color', 'end_color', 'color_text', 'color_icon', 'angle']
    search_fields = ['start_color', 'end_color']
