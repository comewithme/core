from django.apps import AppConfig


class EventsConfig(AppConfig):
    name = 'modules.events'

    def ready(self):
        # Инициализация сигналов
        from . import signals
