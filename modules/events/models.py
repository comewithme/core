from django.db import models

from django.conf import settings
from django.utils import timezone
from urllib.parse import urljoin


class Category(models.Model):
    name = models.CharField(max_length=30, verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class ColorGradient(models.Model):
    start_color = models.CharField(max_length=30, blank=True, null=True, verbose_name='Стартовый цвет карточки')
    end_color = models.CharField(max_length=30, blank=True, verbose_name='Конечный цвет карточки')
    color_text = models.CharField(max_length=30, blank=True, verbose_name='Цвет текста')
    color_icon = models.CharField(max_length=30, blank=True, verbose_name='Цвет иконки')
    angle = models.IntegerField(default=45, verbose_name='Угол')

    def __str__(self):
        return '{0} {1} {2}'.format(self.start_color, self.end_color, self.angle)

    class Meta:
        verbose_name = 'Набор Цвета'
        verbose_name_plural = 'Набор цветов'


class Events(models.Model):
    ALL = 0
    MAN = 1
    WOMAN = 2

    TYPES = (
        (MAN, 'Мужчину'),
        (WOMAN, 'Женщину'),
        (ALL, 'Всех')
    )

    name = models.CharField(max_length=150, verbose_name='Название')
    description = models.TextField(null=True, verbose_name='Описание')
    address = models.CharField(max_length=255, null=True, verbose_name='Адрес')
    color = models.ForeignKey(ColorGradient, blank=True, null=True, on_delete=models.CASCADE,
                              verbose_name='Цветовой набор')
    coordinate_x = models.FloatField(verbose_name='Координата по x', max_length=100,
                                     help_text='Пример 54.7410')
    coordinate_y = models.FloatField(verbose_name='Координата по y', max_length=100,
                                     help_text='Пример 55.9889')
    accept_automatic = models.BooleanField(verbose_name='Автоматически принимать участников?', default=True)
    url_site = models.CharField(verbose_name='Ссылка события', max_length=255, blank=True, null=True)
    is_active = models.BooleanField(verbose_name='Активен?', default=True)
    category = models.ForeignKey(Category, related_name='category', verbose_name='Категория', blank=True, null=True,
                                 on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=5, blank=True, null=True, decimal_places=0, default=0, verbose_name='Цена')
    max_count_users = models.IntegerField(null=True, verbose_name='Макс. число участников', default=100)
    create_event = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='create_event',
                                     verbose_name='Создатель события', on_delete=models.CASCADE)
    list_users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='list_users', blank=True,
                                        verbose_name='Список участников')
    search_gender = models.PositiveSmallIntegerField('Ищу', choices=TYPES, default=ALL, blank=True, null=True,
                                                     db_index=True)
    photo_event = models.FileField(blank=True, null=True, verbose_name='Картинка события')
    is_online = models.BooleanField(default=False, verbose_name='Событие онлайн?')
    date_event = models.DateTimeField(verbose_name='Дата и время события', blank=True)
    date_create_event = models.DateTimeField(verbose_name='Дата создания события', default=timezone.now, null=True)

    def __str__(self):
        return self.name

    def get_photo_event(self):
        if self.photo_event:
            return urljoin(settings.BASE_HOST, self.photo_event.url)
        else:
            # path_photo = urljoin(settings.MEDIA_URL, 'default_chat.png')
            # return urljoin(settings.BASE_HOST, path_photo)
            return ''

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'
