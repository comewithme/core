from django.db.models.signals import post_save
from django.dispatch import receiver
import logging

from fcm_django.models import FCMDevice

from configs import FCM_SERVER_KEY
from modules.events.models import Events
from modules.users.models import Users
from utils.utils import send_push_fcm_device

LOGGER = logging.getLogger(__name__)


@receiver(post_save, sender=Events)
def post_save_events(sender, instance, created, **kwargs):
    if created and instance.is_online:
        devices = FCMDevice.objects.all().exclude(user=instance.create_event)
        send_push_fcm_device(devices, instance.name)
    elif created and not instance.is_online:
        coordinate_x1 = instance.coordinate_x - 1
        coordinate_x2 = instance.coordinate_x + 1
        coordinate_y1 = instance.coordinate_y - 1
        coordinate_y2 = instance.coordinate_y + 2

        users = Users.objects.filter(
            location_by_x__range=(coordinate_x1, coordinate_x2),
            location_by_y__range=(coordinate_y1, coordinate_y2)
        )

        devices = FCMDevice.objects.filter(user__in=users).exclude(user=instance.create_event)
        try:
            send_push_fcm_device(devices, instance.name)
        except Exception as ex:
            LOGGER.error(f'Can not send push:{ex}')