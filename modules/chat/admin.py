from django.contrib import admin

from modules.chat.models import Chat, Message


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_filter = ['personal_chat', ]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass
