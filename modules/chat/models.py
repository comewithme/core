from django.db import models

from configs import settings
from modules.events.models import Events
from modules.users.models import Users
from django.utils import timezone
from urllib.parse import urljoin


class Chat(models.Model):
    users = models.ManyToManyField(Users, verbose_name='Участники чата')
    event = models.ForeignKey(Events, blank=True, null=True, verbose_name='Событие', on_delete=models.CASCADE)
    photo_chat = models.ImageField(blank=True, null=True, default='default_chat.png', verbose_name='Фото')
    personal_chat = models.BooleanField(default=False, verbose_name='Личный чат?')
    date_created = models.DateTimeField(verbose_name='Дата создания чата', default=timezone.now, null=True)

    def __str__(self):
        if not self.personal_chat:
            return f"{self.event if self.event else ''}"
        else:
            return "Личный чат пользователей"

    class Meta:
        verbose_name = "Чат"
        verbose_name_plural = 'Чаты'

    def get_photo_chat(self):
        if self.personal_chat:
            url_photo_chat = str(self.photo_chat)
            return urljoin(settings.BASE_HOST, url_photo_chat)
        elif self.event.photo_event:
            return self.event.get_photo_event()
        else:
            return ''


class Message(models.Model):
    sender = models.ForeignKey(Users, verbose_name='Отправитель', on_delete=models.CASCADE)
    message = models.TextField(verbose_name='Текст сообщения')
    to_chat = models.ForeignKey(Chat, verbose_name='Чат', on_delete=models.CASCADE)
    date_created = models.DateTimeField(verbose_name='Дата создания чата', default=timezone.now, null=True)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = 'Сообщения'
        ordering = ('-date_created',)

    def __str__(self):
        return f"{self.sender} отправил сообщение: {self.message[:50]}"
