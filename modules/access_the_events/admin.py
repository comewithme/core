from django.contrib import admin

from modules.access_the_events.models import AccessForEvents


@admin.register(AccessForEvents)
class AccessForEvents(admin.ModelAdmin):
    list_display = ['id', 'user', 'event', 'access']
    search_fields = ['user', ]
    raw_id_fields = ['user', 'event']
