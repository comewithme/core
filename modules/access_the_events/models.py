from django.db import models
from django.conf import settings

from modules.events.models import Events


class AccessForEvents(models.Model):
    DOES_NOT_GO = 0
    GOES = 1
    NO_READ = 2
    CREATED = 3
    AWAITING_CONFIRMATION = 4
    REJECTED_CREATOR = 5
    COMPLETED = 6

    TYPES = (
        (DOES_NOT_GO, 'Не идет'),
        (GOES, 'Идет'),
        (NO_READ, 'Не просмотрено'),
        (CREATED, 'Создатель'),
        (AWAITING_CONFIRMATION, 'Ожидает подтверждения'),
        (REJECTED_CREATOR, 'Отклонено'),
        (COMPLETED, 'Завершено')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user',
                             verbose_name='Пользователь', on_delete=models.CASCADE)
    event = models.ForeignKey(Events, on_delete=models.CASCADE, verbose_name='Событие')
    access = models.PositiveSmallIntegerField('Доступ', choices=TYPES, default=NO_READ, blank=True, null=True,
                                              db_index=True)

    def __str__(self):
        return '{0}, {1} - {2}'.format(self.user, self.event, self.access)

    class Meta:
        verbose_name = 'Доступ к событию'
        verbose_name_plural = 'Доступ к событиям'
