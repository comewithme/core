from django.apps import AppConfig


class AccessTheEventsConfig(AppConfig):
    name = 'access_the_events'
