# Generated by Django 3.1.5 on 2021-01-25 16:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('access_the_events', '0003_accessforevents_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessforevents',
            name='access',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'Не идет'), (1, 'Идет'), (2, 'Не просмотрено'), (3, 'Создатель'), (4, 'Ожидает подтверждения'), (5, 'Отклонено'), (6, 'Завершено')], db_index=True, default=2, null=True, verbose_name='Доступ'),
        ),
    ]
