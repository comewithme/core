from django.views.generic import TemplateView
import logging

LOGGER = logging.getLogger(__name__)


class StartView(TemplateView):
    template_name = 'start.html'
