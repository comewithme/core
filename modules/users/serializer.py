from rest_framework import serializers

from modules.users.models import Users


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['id',
                  'email',
                  'first_name',
                  'birthday',
                  'phone',
                  'gender',
                  'photo',
                  'about_me',
                  ]
