from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager

from configs import settings
from modules.ratings.models import UserRatings
from urllib.parse import urljoin


class CustomUserManager(BaseUserManager):
    """
    Custom user model manager where phone is the unique identifiers
    for authentication instead of usernames.
    """

    def _create_user(self, phone, password, **extra_fields):
        """Create and save a User in DB with the given phone"""
        if not phone:
            raise ValueError('Phone number must be provided')
        user = self.model(phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password=None, **extra_fields):
        """Create and save a regular User with the given phone and password"""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password, **extra_fields):
        """Create and save a Superuser with the given phone and password"""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')
        return self._create_user(phone, password, **extra_fields)


class Users(AbstractUser):
    ALL = 0
    MAN = 1
    WOMAN = 2

    TYPES = (
        (MAN, 'Мужчина'),
        (WOMAN, 'Женщина'),
        (ALL, 'Не определился'),
    )

    username = None
    last_name = None
    email = None
    first_name = models.CharField(max_length=100, blank=True, null=True, verbose_name='Имя')
    birthday = models.DateField(blank=True, null=True, verbose_name='Дата рождения')
    phone = models.CharField(max_length=30, blank=True, null=True, unique=True, verbose_name='Телефон')
    vk = models.CharField(max_length=30, blank=True, null=True, verbose_name='Вконтакте')
    instagram = models.CharField(max_length=30, blank=True, null=True, verbose_name='Инстаграм')
    telegram = models.CharField(max_length=30, blank=True, null=True, verbose_name='Телеграм')
    gender = models.PositiveSmallIntegerField('Пол', choices=TYPES, default=ALL, db_index=True)
    photo = models.ImageField(blank=True, null=True, default='avatar.png', verbose_name='Фото')
    about_me = models.TextField(verbose_name='Обо мне', blank=True, null=True)
    location_by_x = models.FloatField(verbose_name='Координата по x', max_length=50, null=True, blank=True,
                                      help_text='Пример 54.7410')
    location_by_y = models.FloatField(verbose_name='Координата по y', max_length=50, null=True, blank=True,
                                      help_text='Пример 55.9889')
    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        if self.first_name:
            return str(self.first_name)
        else:
            return str(self.phone)

    def get_photo(self):
        if self.photo:
            return urljoin(settings.BASE_HOST, self.photo.url)
        else:
            return ''

    def get_total_rating(self):
        try:
            rating = UserRatings.objects.get(user=self.id)
            return rating.total_popularity
        except:
            total_rating = UserRatings()
            total_rating.user = self
            total_rating.total_popularity = 0.0
            total_rating.number_events_visited = 0
            total_rating.save()
            return 0.0

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
