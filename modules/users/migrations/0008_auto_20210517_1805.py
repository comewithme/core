# Generated by Django 3.1.5 on 2021-05-17 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20210425_1726'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='email',
        ),
        migrations.AlterField(
            model_name='users',
            name='gender',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Мужчина'), (2, 'Женщина'), (0, 'Не определился')], db_index=True, default=0, verbose_name='Пол'),
        ),
    ]
