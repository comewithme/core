from django.contrib import admin

from modules.users.models import Users


@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'birthday', 'phone', 'photo', 'about_me', 'gender']
    search_fields = ['email', 'phone', ]

