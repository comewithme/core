from django.urls import path, include

urlpatterns = [
    path('v0/', include('modules.api.v0.urls'), name='v0'),
]
