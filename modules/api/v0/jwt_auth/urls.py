from django.urls import path
from .views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

urlpatterns = [
    path('token-auth/', obtain_jwt_token, name='token-auth'),
    path('token-verify/', verify_jwt_token, name='token-verify'),
    path('token-refresh/', refresh_jwt_token, name='token-refresh'),
]
