from django.core.cache import cache
from rest_framework.permissions import AllowAny
from rest_framework_jwt.views import JSONWebTokenAPIView
from rest_framework.response import Response
from rest_framework import status

from modules.users.models import Users
from .utils import check_token, get_jwt_token, refresh_token
from utils.answer import NO_TOKEN, TOKEN_NOT_VALID, INCORRECT_PASSWORD, MISSING_DATA
from utils.regulars import login as login_re

__all__ = ['ObtainJWTToken', 'CheckJWTToken', 'RefreshJWTToken']

from ..base_api_view import BaseAPIView


class ObtainJWTToken(BaseAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    """
    Создание токена
    """
    BLACK_LIST = "black_auth_{}"

    def post(self, request, *args, **kwargs):
        """
        Авторизация по логину и паролю
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        credentials = {
            "username": request.data.get("phone"),
            "password": request.data.get("password")
        }
        if all(credentials.values()):
            login = login_re.sub("", credentials.get("username"))
            password = login_re.sub("", credentials.get("password"))
            if self.__check_black_list(login):
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                data='Вы многократно ввели неверный пароль. Авторизация в данный момент запрещена. Повторите попытку через 60 минут.')
            try:
                user = Users.objects.get(phone=login)
                user_pass = user.check_password(password)
            except:
                user_pass = False
            if user_pass:
                result = get_jwt_token(user).get('tokens')
                if "error_message" not in result.keys():
                    self.__clean_black_list(login)
                    return Response(status=status.HTTP_200_OK,
                                    data=self.get_answer(data=result))
                else:
                    return Response(status=status.HTTP_404_NOT_FOUND,
                                    data=self.get_answer(error='error'))
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=self.get_answer(error=INCORRECT_PASSWORD))

        return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=MISSING_DATA))

    def __check_black_list(self, login) -> bool:
        """
        Проверка на брутфорс(перебор) пароля
        Если клиент более N раз ввел неправильный пароль, то блокируем его на несколько минут
        """
        count = cache.get(self.BLACK_LIST.format(login), 0)
        if count > 30:
            return True
        count += 1
        # Сохраняем счетчик на 1 час
        cache.set(self.BLACK_LIST.format(login), count, 3600 * 60)
        return False

    def __clean_black_list(self, login):
        """
        Очистка черного списка после успешной авторизации
        """
        cache.delete(self.BLACK_LIST.format(login))


class CheckJWTToken(JSONWebTokenAPIView):
    """
    Проверка токена
    """

    def post(self, request, *args, **kwargs):
        token = request.data.get("token", None)
        if token is not None:
            data = check_token(token)
            if data is not None:
                return Response(status=status.HTTP_200_OK, data=data)
            else:
                return Response(status=TOKEN_NOT_VALID, data=TOKEN_NOT_VALID)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=NO_TOKEN)


class RefreshJWTToken(JSONWebTokenAPIView):
    """
    Обновление access токена по refresh токену
    """

    def post(self, request, *args, **kwargs):
        token = request.data.get("token", None)
        answer = {"status": "error", "detail": {}, "error_message": None}
        if token is not None:
            new_token = refresh_token(token)
            if new_token is not None:
                return Response(status=status.HTTP_200_OK, data=new_token)

            else:
                return Response(status=status.HTTP_404_NOT_FOUND, data=answer)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=NO_TOKEN)


obtain_jwt_token = ObtainJWTToken.as_view()
verify_jwt_token = CheckJWTToken.as_view()
refresh_jwt_token = RefreshJWTToken.as_view()
