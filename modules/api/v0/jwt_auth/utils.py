import jwt
import logging
from django.conf import settings
from datetime import datetime
from datetime import timedelta

__all__ = ['get_jwt_token', 'refresh_token', 'check_token']
LOGGER = logging.getLogger("django")


def check_token(token):
    """
    Проверка токена
    :param token:
    :return:
    """
    claims = jwt.decode(token, verify=False)
    if not isinstance(claims, dict) or claims.get('exp') <= int(datetime.now().timestamp()):
        # Если токен не имеет нужных полей или не тот тип или истек по времени жизни - 403
        return None
    if not claims.get("type"):
        return None
    if not claims.get("user_id"):
        return None
    try:
        new_claims = jwt.decode(token, settings.JWT_SECRET_KEY)
    except Exception as ex:
        LOGGER.error("Error of check token:{}".format(ex))
        return None
    if new_claims is None:
        return None
    return new_claims


def refresh_token(token):
    """
    Верификация access токена
    :param token:
    :return:
    """
    try:
        data = check_token(token)
    except Exception as ex:
        LOGGER.error("Error of check access token:{}".format(ex))
        return None
    if data is None:
        return None
    if data.get('type') != 'refresh':
        return None
    data["exp"] = datetime.now() + timedelta(minutes=settings.EXP_LIFE_TIME)
    data["type"] = "access"
    token = jwt.encode(data, settings.JWT_SECRET_KEY, algorithm='HS256')
    data["exp"] = datetime.now() + timedelta(days=settings.EXP_REFRESH_TIME)
    data["type"] = "refresh"
    refresh = jwt.encode(data, settings.JWT_SECRET_KEY, algorithm='HS256')
    return {"access": token, "refresh": refresh}


def get_jwt_token(user):
    """
    Генерация токена для пользователя
    :param exp:
    :param images:
    :return:
    """
    if user is not None:
        result = dict()
        exp = timedelta(minutes=settings.EXP_LIFE_TIME)
        now = datetime.now()
        payload = {
            "exp": now + exp,
            "user_id": user.id,
            "type": "access"
        }
        access = jwt.encode(payload, settings.JWT_SECRET_KEY, algorithm='HS256')
        exp = timedelta(days=settings.EXP_REFRESH_TIME)
        now = datetime.now()
        payload["exp"] = now + exp
        payload["type"] = "refresh"

        refresh = jwt.encode(payload, settings.JWT_SECRET_KEY, algorithm='HS256')
        result["tokens"] = {"access": access, "refresh": refresh}
    else:
        result = {"error": "User not found"}

    return result
