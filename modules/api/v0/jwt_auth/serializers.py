from rest_framework import serializers
from rest_framework_jwt.compat import Serializer
from rest_framework_jwt.settings import api_settings
from .utils import get_jwt_token

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class JSONWebTokenSerializer(Serializer):

    def __init__(self, *args, **kwargs):
        super(JSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields['user_id'] = serializers.CharField(max_length=255)
        self.fields['prov_id'] = serializers.CharField(max_length=255)
        self.fields[''] = serializers.CharField(max_length=255)

    def validate(self, attrs):
        login = attrs.get('login')
        password = attrs.get('password')
        if all([login, password]):
            tokens = get_jwt_token(login, password)
            if tokens is None:
                msg = 'Auth error'
                raise serializers.ValidationError(msg)
            return tokens
        else:
            raise serializers.ValidationError("Login and password required")
