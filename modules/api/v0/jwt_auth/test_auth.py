from django.urls import reverse
from rest_framework.test import APIRequestFactory, APITestCase
from django.test import TestCase
from modules.api.v0.jwt_auth.views import ObtainJWTToken, obtain_jwt_token
from modules.users.models import Users


class TestAuth(TestCase):
    """
    Тестирование авторизации
    """

    def setUp(self) -> None:
        self.login = Users.objects.create_user(
            email="test_access_object@user.com",
            password="test_access_object_password"
        )

    def test_post_auth(self):
        """
        проверка авторизации
        :return:
        """
        factory = APIRequestFactory()
        request = factory.post(
            reverse('token-auth'), data={"login": "test_access_object@user.com", "password": "test_access_object_password"}
        )
        response = obtain_jwt_token(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get('detail'))
        self.assertEqual(response.data.get('status'), 'ok')
