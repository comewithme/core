from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from modules.api.v0.jwt_auth.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token
from modules.users.models import Users


class TestJWTAuth(TestCase):

    def setUp(self) -> None:
        self.login = Users.objects.create_user(
            email="test_user@user.com",
            password="test_password"
        )

    def test_get_token(self):
        factory = APIRequestFactory()
        request = factory.post(
            reverse('token-auth'), data={"login": "test_user@user.com", "password": "test_password"}
        )
        response = obtain_jwt_token(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get("detail").get("tokens"))
        self.assertIsNotNone(response.data.get("detail").get("tokens").get("access"))
        self.assertIsNotNone(response.data.get("detail").get("tokens").get("refresh"))

    def test_symbols(self):
        """
        Проверка что все спецсиволы удалились из логина и пароля
        """
        factory = APIRequestFactory()
        request = factory.post(
            reverse('token-auth'), data={"login": "test_user@user.com", "password": "test_password"}
        )
        response = obtain_jwt_token(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get("detail").get("tokens"))
        self.assertIsNotNone(response.data.get("detail").get("tokens").get("access"))
        self.assertIsNotNone(response.data.get("detail").get("tokens").get("refresh"))

    def test_verify_token(self):
        factory = APIRequestFactory()
        request = factory.post(
            reverse('token-auth'), data={"login": "test_user@user.com", "password": "test_password"}
        )
        response = obtain_jwt_token(request)

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get("detail").get("tokens"))

        token = response.data.get("detail").get("tokens").get("access").decode("utf-8")
        request = factory.post(
            reverse('token-verify'), data={"token": token}
        )
        response = verify_jwt_token(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get("detail").get("valid"), True)

    def test_refresh_access_token(self):
        factory = APIRequestFactory()
        request = factory.post(
            reverse('token-auth'), data={"login": "test_user@user.com", "password": "test_password"}
        )
        response = obtain_jwt_token(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get("detail").get("tokens"))

        token = response.data.get("detail").get("tokens").get("refresh").decode("utf-8")
        request = factory.post(
            reverse('token-refresh'), data={"token": token}
        )
        response = refresh_jwt_token(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get("detail").get("access"))
        self.assertIsNotNone(response.data.get("detail").get("refresh"))

    def test_bruteforce(self):
        """
        Проверка защиты от перебора поролей
        """
        factory = APIRequestFactory()
        for i in range(50):
            request = factory.post(
                reverse('token-auth'), data={"login": "test_user1", "password": "test_password1234567" + str(i)}
            )
            response = obtain_jwt_token(request)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data.get("error_message"),
                         "Вы многократно ввели неверный пароль. Авторизация в данный момент запрещена. Повторите попытку через 60 минут.")