from django.urls import reverse
from rest_framework.test import APIRequestFactory
from django.test import TestCase
from datetime import datetime
import os
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile


class TestEvent(TestCase):
    def test_post_register(self):
        """
        правильное заполнение полей регистрации
        :return:
        """
        factory = APIRequestFactory()
        request = factory.post(
            reverse('api_register'),
            data={
                'name': "Test",
                'email': "test@test.ru",
                'phone': "89998887766",
                'birthday': 947703600,
                'gender': 0,
                'password': "1234",
                'repeat_password': "1234",
            }
        )
        view = RegisterView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get('detail'))
        self.assertEqual(response.data.get('status'), 'ok')


    def test_post_register_error(self):
        """
        правильное заполнение полей регистрации
        :return:
        """
        factory = APIRequestFactory()
        request = factory.post(
            reverse('api_register'),
            data={
                'name': "Test",
                'email': "test@test.ru",
                'phone': "89998887766",
                'birthday': 947703600,
                'gender': 0
            }
        )
        view = RegisterView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.get('status'), 'error')
