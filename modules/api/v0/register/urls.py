from django.urls import path

from modules.api.v0.register.check_code import CheckCodeAPI
from modules.api.v0.register.check_phone_and_send_code import CheckPhoneAndSendCodeAPI
from modules.api.v0.register.registration import RegistrationAPI

urlpatterns = [
    path('check_phone_and_send_code/', CheckPhoneAndSendCodeAPI.as_view(), name='check_phone_and_send_code'),
    path('check_code/', CheckCodeAPI.as_view(), name='check_code'),
    path('registration/', RegistrationAPI.as_view(), name='complaint_event'),
]
