import logging
from rest_framework.response import Response
from rest_framework import status
from rest_framework.test import APIRequestFactory

from modules.api.v0.base_api_view import BaseAPIView
from utils.answer import MISSING_DATA, SUCCESS_CODE, CODE_NOT_VALID
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from configs import settings

factory = APIRequestFactory()

__all__ = ['CheckCodeAPI']

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
LOGGER = logging.getLogger(__name__)


class CheckCodeAPI(BaseAPIView):
    authentication_classes = ()
    """
    Проверка кода из смс
    """

    def post(self, request, *args, **kwargs):
        """
        Проверка кода из смс
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        code = request.data.get('code')
        LOGGER.info(f"code in check_code: {code}")

        if not all((code,)):
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=MISSING_DATA))
        try:
            code = int(code)
            LOGGER.info(f"code: {code} and type {type(code)}")
        except:
            LOGGER.error(f"Can't convert in int: {code}")

        try:
            for key in cache.keys('*'):
                if isinstance(cache.get(key), dict):
                    if cache.get(key)['code'] == code:
                        cache.delete(key)
                        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SUCCESS_CODE))
        except Exception as ex:
            LOGGER.error(f'Error: {ex}')

        return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=CODE_NOT_VALID))
