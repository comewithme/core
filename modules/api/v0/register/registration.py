import logging
from rest_framework.response import Response
from rest_framework import status

from django.urls import reverse

from rest_framework.test import APIRequestFactory
from modules.api.v0.base_api_view import BaseAPIView
from modules.api.v0.jwt_auth.views import ObtainJWTToken
from modules.ratings.models import UserRatings
from utils.answer import MISSING_DATA, INCORRECT_PASSWORD_REGISTER, SERVICE_UNAVAILABLE, INCORRECT_PHONE
from utils.regulars import login as login_re
from modules.users.models import Users

factory = APIRequestFactory()

__all__ = ['RegistrationAPI']

LOGGER = logging.getLogger(__name__)


class RegistrationAPI(BaseAPIView):
    authentication_classes = ()
    """
    Проверка номера и отправка смс
    """

    def post(self, request, *args, **kwargs):
        """
        Проверка номера и отправка смс
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        name = request.data.get('name')
        phone = request.data.get('phone')
        gender = request.data.get('gender')
        password = request.data.get('password')
        repeat_password = request.data.get('repeat_password')

        if not all((name, password, repeat_password, phone)):
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=MISSING_DATA))

        password = login_re.sub("", password)
        repeat_password = login_re.sub("", repeat_password)

        if password != repeat_password:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_answer(error=INCORRECT_PASSWORD_REGISTER))

        if Users.objects.filter(phone=phone):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_answer(error=INCORRECT_PHONE))

        if gender == 1:
            gender = Users.MAN
        elif gender == 2:
            gender = Users.WOMAN
        else:
            gender = Users.ALL

        try:
            user_register = Users()
            user_register.first_name = name
            user_register.phone = phone
            user_register.gender = gender
            user_register.set_password(password)
            user_register.save()

            UserRatings.objects.create(user=user_register,
                                       total_popularity=0,
                                       number_events_visited=0)
        except Exception as ex:
            LOGGER.error('Пользователь не создан потому что {0}'.format(ex))
            return Response(
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                data=self.get_answer(error=SERVICE_UNAVAILABLE)
            )

        try:
            request = factory.post(reverse('token-auth'), {'phone': phone,
                                                           'password': password})
            view = ObtainJWTToken.as_view()
            response = view(request)
            answer = response.data.get('detail')
        except Exception as ex:
            LOGGER.error('Ошибка получение токена - {0}'.format(ex))

        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=answer))
