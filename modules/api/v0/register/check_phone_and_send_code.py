import logging
from rest_framework.response import Response
from rest_framework import status
from rest_framework.test import APIRequestFactory

from modules.api.v0.base_api_view import BaseAPIView
from utils.answer import MISSING_DATA, INCORRECT_PHONE, SEND_CODE, MANY_SEND_SMS
from modules.users.models import Users
from utils.utils import send_sms
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from configs import settings

import random

factory = APIRequestFactory()

__all__ = ['CheckPhoneAndSendCodeAPI']
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
LOGGER = logging.getLogger(__name__)


class CheckPhoneAndSendCodeAPI(BaseAPIView):
    authentication_classes = ()
    """
    Проверка номера и отправка смс
    """

    def post(self, request, *args, **kwargs):
        """
        Проверка номера и отправка смс
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        phone = request.data.get('phone')
        random_number = random.randint(1000, 9999)
        message = f"Код подтверждения: {random_number}"
        if not all((phone,)):
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=MISSING_DATA))

        if Users.objects.filter(phone=phone):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_answer(error=INCORRECT_PHONE))

        try:
            # save in cache
            if not cache.get(message):
                if cache.get(phone):
                    if cache.get(phone)["count"] < 3:
                        new_count = cache.get(phone)['count'] + 1
                        value = {"code": random_number, "count": new_count}
                        cache.set(phone, value, 300)
                        LOGGER.info(f'Cached for {phone}: {cache.get(phone)}')
                        send_sms(phone, message)
                        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SEND_CODE))
                    else:
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=MANY_SEND_SMS))

                value = {"code": random_number, "count": 1}
                cache.set(phone, value, 300)
                LOGGER.info(f'Cached for {phone}: {cache.get(phone)}')
                send_sms(phone, message)
        except Exception as ex:
            LOGGER.error(ex)

        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SEND_CODE))
