import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import ViewAPI
from modules.events.models import ColorGradient

__all__ = ['ColorGradientsApi', ]

from modules.events.serializer.color_gradient_serializer import ColorGradientSerializer

from utils.answer import NO_DATA

LOGGER = logging.getLogger(__name__)


class ColorGradientsApi(ViewAPI):

    def get(self, request, *args, **kwargs):
        """Список всех цветовых градиентов."""

        color_gradients = ColorGradient.objects.all()
        if color_gradients is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(NO_DATA)
            )

        serializer = ColorGradientSerializer(color_gradients, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=serializer.data)
        )
