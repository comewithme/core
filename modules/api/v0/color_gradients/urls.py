from django.urls import path

from modules.api.v0.color_gradients.color_gradients import ColorGradientsApi

urlpatterns = [
    path('', ColorGradientsApi.as_view(), name='color_gradients'),
]
