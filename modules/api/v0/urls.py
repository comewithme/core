from django.urls import path, include


urlpatterns = [
    path('auth/', include('modules.api.v0.jwt_auth.urls'), name='jwt'),
    path('register/', include('modules.api.v0.register.urls'), name='api_register'),
    path('user/', include('modules.api.v0.users.urls'), name='user'),
    path('event/', include('modules.api.v0.events.urls'), name='event'),
    path('category/', include('modules.api.v0.category.urls'), name='tags'),
    path('feedback/', include('modules.api.v0.feedback.urls'), name='feedback'),
    path('color_gradients/', include('modules.api.v0.color_gradients.urls'), name='color_gradients'),
    path('complaint/', include('modules.api.v0.complaint.urls'), name='complaint'),
    path('city/', include('modules.api.v0.city.urls'), name='city'),
    path('chats/', include('modules.api.v0.chat.urls'), name='chats'),
]
