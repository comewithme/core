import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.ratings.models import Reviews, UserRatings
from modules.users.models import Users
from utils.answer import MISSING_DATA, USER_NOT_FOUND
import time
import datetime

__all__ = ['InfoUserAndRatingAPI', ]

LOGGER = logging.getLogger(__name__)


class InfoUserAndRatingAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Информация о пользователе и список отзывов
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user_id = self.request.query_params.get('user_id')

        answer = {}

        if not all((user_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            user = Users.objects.get(id=user_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=USER_NOT_FOUND)
            )
        try:
            rating_user = UserRatings.objects.get(user=user)
            rating_user = rating_user.total_popularity
        except:
            rating_user = 0.0

        info_user = {
            'id': user.id,
            'name': user.first_name,
            'birthday': int(time.mktime(datetime.datetime.strptime(f'{user.birthday}',
                                                                   "%Y-%m-%d").timetuple())) if user.birthday else 0,
            'phone': user.phone,
            'gender': user.gender,
            'vk': user.vk if user.vk else '',
            'instagram': user.instagram if user.instagram else '',
            'telegram': user.telegram if user.telegram else '',
            'photo': user.get_photo(),
            'about_me': user.about_me if user.about_me else "",
            'total_rating': rating_user
        }
        list_feedback_user = []

        list_rating = Reviews.objects.filter(user_to=user_id)

        for rating in list_rating:
            list_feedback_user.append({
                'user_from': {
                    'id': rating.user_from.id,
                    'name': rating.user_from.first_name,
                    'gender': rating.user_from.gender,
                    'vk': rating.user_from.vk if rating.user_from.vk else '',
                    'instagram': rating.user_from.instagram if rating.user_from.instagram else '',
                    'telegram': rating.user_from.telegram if rating.user_from.telegram else '',
                    'birthday': int(time.mktime(datetime.datetime.strptime(f'{rating.user_from.birthday}',
                                                                           "%Y-%m-%d").timetuple())) if rating.user_from.birthday else 0,
                    'photo': rating.user_from.get_photo(),
                },
                'star_for_event': rating.star_for_event,
                'description': rating.description,
                'date_create': int(rating.date_create.timestamp()) if rating.date_create else 0
            })

        answer.update({
            "info_user": info_user,
            'list_feedback_user': list_feedback_user
        })

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(answer)
        )
