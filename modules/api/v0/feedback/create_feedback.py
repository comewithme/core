import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from modules.events.models import Events
from modules.ratings.models import Reviews, UserRatings
from modules.users.models import Users
from utils.answer import MISSING_DATA, NOT_FOUND_EVENT, SUCCESS_CREATE_FEEDBACK, USER_NOT_FOUND, ERROR_CREATE_FEEDBACK

__all__ = ['CreateFeedback', ]

LOGGER = logging.getLogger(__name__)


class CreateFeedback(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Создание отзыва о пользователе
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = request.data.get('event_id')
        user_id = request.data.get('user_id')
        appraisal = int(request.data.get('appraisal'))
        description = request.data.get('description')

        if not all((event_id, user_id, appraisal)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            event = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NOT_FOUND_EVENT)
            )
        try:
            user = Users.objects.get(id=user_id)
        except:
            return Response(
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                data=self.get_answer(error=USER_NOT_FOUND)
            )

        if appraisal == 1:
            appraisal = Reviews.ONE
        elif appraisal == 2:
            appraisal = Reviews.TWO
        elif appraisal == 3:
            appraisal = Reviews.THREE
        elif appraisal == 4:
            appraisal = Reviews.FOUR
        elif appraisal == 5:
            appraisal = Reviews.FIVE

        if Reviews.objects.filter(user_from=self.request.user,
                                  user_to=user,
                                  event=event,
                                  star_for_event=appraisal):
            return Response(
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                data=self.get_answer(error=ERROR_CREATE_FEEDBACK)
            )

        review = Reviews.objects.create(user_from=self.request.user,
                                        user_to=user,
                                        event=event,
                                        star_for_event=appraisal,
                                        description=description)

        try:
            access_on_event = AccessForEvents.objects.get(user=user,
                                                          event=event)
            access_on_event.access = AccessForEvents.COMPLETED
            access_on_event.save()
        except:
            access_on_event = AccessForEvents()
            access_on_event.user = user
            access_on_event.event = event
            access_on_event.access = AccessForEvents.COMPLETED
            access_on_event.save()

        # общая сумма оценок
        sum_rating = 0

        # количество событий
        count_ratings = Reviews.objects.filter(user_to=user).count()

        # список всех оценок пользователей
        ratings = Reviews.objects.filter(user_to=user).values_list('star_for_event')

        for rev in ratings:
            sum_rating += rev[0]

        # средняя оценка пользователя
        total_middle_rating = sum_rating / count_ratings

        try:
            user_rating = UserRatings.objects.get(user=user)
            user_rating.total_popularity = total_middle_rating
            user_rating.number_events_visited += 1
            user_rating.save()
            user_rating.list_reviews.add(review)
        except:
            user_rating = UserRatings()
            user_rating.user = user
            user_rating.total_popularity = appraisal
            user_rating.number_events_visited = 1
            user_rating.save()
            user_rating.list_reviews.add(review)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_CREATE_FEEDBACK)
        )
