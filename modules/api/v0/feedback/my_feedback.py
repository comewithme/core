import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.ratings.models import Reviews
from modules.users.models import Users
from utils.answer import USER_NOT_FOUND
import time
import datetime

__all__ = ['MyRatingAPI', ]

LOGGER = logging.getLogger(__name__)


class MyRatingAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Список отзывов
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        answer = {}
        try:
            user = Users.objects.get(id=self.request.user.id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=USER_NOT_FOUND)
            )

        list_feedback_user = []

        list_rating = Reviews.objects.filter(user_to=self.request.user)

        for rating in list_rating:
            list_feedback_user.append({
                'user_from': {
                    'id': rating.user_from.id,
                    'name': rating.user_from.first_name if rating.user_from.first_name else '',
                    'birthday': int(time.mktime(datetime.datetime.strptime(f'{rating.user_from.birthday}',
                                                                           "%Y-%m-%d").timetuple())) if rating.user_from.birthday else 0,
                    'vk': rating.user_from.vk if rating.user_from.vk else '',
                    'instagram': rating.user_from.instagram if rating.user_from.instagram else '',
                    'telegram': rating.user_from.telegram if rating.user_from.telegram else '',
                },
                'star_for_event': rating.star_for_event,
                'description': rating.description if rating.description else '',
                'date_create': int(rating.date_create.timestamp())
            })

            answer.update({
                'list_feedback_user': list_feedback_user
            })

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(answer)
        )
