from django.urls import path

from modules.api.v0.feedback.create_feedback import CreateFeedback
from modules.api.v0.feedback.info_user_feedback import InfoUserAndRatingAPI
from modules.api.v0.feedback.my_feedback import MyRatingAPI

urlpatterns = [
    path('create/', CreateFeedback.as_view(), name='feedback_create'),
    path('info_user_feedback/', InfoUserAndRatingAPI.as_view(), name='info_user_feedback'),
    path('my_feedback/', MyRatingAPI.as_view(), name='my_feedback'),
]
