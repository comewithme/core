import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from modules.api.v0.events.list_my_events import get_info_access_event
from django.utils import timezone

__all__ = ['HistoryAcceptedEventsAPI', ]

from modules.events.models import Events
from utils.answer import EVENT_NOT_FOUND

LOGGER = logging.getLogger(__name__)


class HistoryAcceptedEventsAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        история событий, на которые я ходил
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        now = timezone.now()

        list_accepted_events = AccessForEvents.objects.filter(user=self.request.user,
                                                              event__date_event__lt=now,
                                                              event__is_active=True,
                                                              access__in=[AccessForEvents.COMPLETED,
                                                                          AccessForEvents.GOES])

        for accept_event in list_accepted_events:
            event = Events.objects.get(id=accept_event.event.id)

            if event.create_event == self.request.user:
                continue

            ac_event = AccessForEvents.objects.get(event=event,
                                                   user=self.request.user)
            ac_event.access = AccessForEvents.COMPLETED
            ac_event.save()

        if not list_accepted_events:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data=self.get_answer(error=EVENT_NOT_FOUND)
            )

        answer_api = get_info_access_event(list_accepted_events)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )
