import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat
from modules.events.models import Events
from modules.users.models import Users
from utils.answer import MISSING_DATA, NOT_FOUND_EVENT, ACCESS_DENIED, SUCCESS_DELETE_USER_FROM_EVENT

__all__ = ['DeleteUserFromEventAPI', ]

LOGGER = logging.getLogger(__name__)


class DeleteUserFromEventAPI(BaseAPIView):

    def delete(self, request, *args, **kwargs):
        """
        Удаление пользователя в событие в качестве участника
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = self.request.data.get('event_id')
        user_id = self.request.data.get('user_id')

        if not all((event_id, user_id)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            event = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NOT_FOUND_EVENT)
            )

        if event.create_event != request.user:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ACCESS_DENIED)
            )

        user = Users.objects.get(id=user_id)
        event.list_users.remove(user)

        try:
            access = AccessForEvents.objects.get(user=user,
                                                 event_id=event_id)
            access.access = AccessForEvents.REJECTED_CREATOR
            access.save()
        except:
            access = AccessForEvents()
            access.user = user_id
            access.event = event_id
            access.access = AccessForEvents.REJECTED_CREATOR
            access.save()

        try:
            chat = Chat.objects.get(event__id=event.id)
            chat.users.remove(user)

        except Exception as ex:
            LOGGER.error(f"[ERROR] Can not remove user:{user} from {event}: {ex}")
        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(SUCCESS_DELETE_USER_FROM_EVENT)
        )
