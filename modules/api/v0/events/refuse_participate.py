import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from modules.events.models import Events
from modules.users.models import Users
from utils.answer import MISSING_DATA, NOT_FOUND_EVENT, SUCCESS_DELETE_USER_FROM_EVENT

__all__ = ['RefuseParticipateAPI', ]

LOGGER = logging.getLogger(__name__)


class RefuseParticipateAPI(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Отказаться от участия в событии
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        event_id = self.request.data.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            events = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NOT_FOUND_EVENT)
            )

        user = Users.objects.get(id=self.request.user.id)
        events.list_users.remove(user)
        try:
            access = AccessForEvents.objects.get(user=user,
                                                 event_id=event_id)
            access.access = AccessForEvents.DOES_NOT_GO
            access.save()
        except:
            access = AccessForEvents()
            access.user = user
            access.event = event_id
            access.access = AccessForEvents.DOES_NOT_GO
            access.save()

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(SUCCESS_DELETE_USER_FROM_EVENT)
        )
