import logging
from rest_framework.response import Response
from rest_framework import status

from configs import settings
from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
import time
import datetime

__all__ = ['ListAcceptUserAndWaitMModifyAPI', ]

from modules.events.models import Events
from modules.ratings.models import UserRatings
from modules.users.models import Users

from utils.answer import MISSING_DATA, NOT_FOUND_EVENT

LOGGER = logging.getLogger(__name__)


class ListAcceptUserAndWaitMModifyAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        списка участников подтверждённых и ожидают подтверждения
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = self.request.query_params.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            event = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NOT_FOUND_EVENT)
            )

        accepted_users = AccessForEvents.objects.filter(event=event,
                                                        access__in=[AccessForEvents.GOES, ])
        list_wait_modify = AccessForEvents.objects.filter(event=event,
                                                          access__in=[AccessForEvents.AWAITING_CONFIRMATION, ])

        info_event_creator = AccessForEvents.objects.select_related('user') \
            .filter(event=event, access__in=[AccessForEvents.CREATED, ]).values_list('user')

        accepted_events = []
        wait_modify = []

        info_creator = Users.objects.get(id=list(info_event_creator)[0][0])
        event_creator = {'id': info_creator.id,
                         'name': info_creator.first_name if info_creator.first_name else '',
                         'birthday': int(time.mktime(datetime.datetime.strptime(f'{info_creator.birthday}',
                                                                                "%Y-%m-%d").timetuple())) if info_creator.birthday else 0,
                         'photo': info_creator.get_photo(),
                         'total_rating': info_creator.get_total_rating(),
                         }

        for accept in accepted_users:
            try:
                total_rating = UserRatings.objects.get(user=accept.user.id)
            except:
                total_rating = UserRatings()
                total_rating.user = accept.user
                total_rating.total_popularity = 0.0
                total_rating.number_events_visited = 0
                total_rating.save()

            accepted_events.append({'id': accept.user.id,
                                    'name': accept.user.first_name if accept.user.first_name else '',
                                    'birthday': int(time.mktime(datetime.datetime.strptime(f'{accept.user.birthday}',
                                                                                           "%Y-%m-%d").timetuple())) if accept.user.birthday else 0,
                                    'photo': '{0}{1}'.format(settings.BASE_HOST, accept.user.photo.url),
                                    'total_rating': total_rating.total_popularity,
                                    })

            list_wait_modify = AccessForEvents.objects.filter(event=event,
                                                              access__in=[AccessForEvents.AWAITING_CONFIRMATION, ])

        for us in list_wait_modify:
            try:
                total_rating = UserRatings.objects.get(user=us.user.id)
            except:
                total_rating = UserRatings()
                total_rating.user = us.user
                total_rating.total_popularity = 0.0
                total_rating.number_events_visited = 0
                total_rating.save()

            wait_modify.append({'id': us.user.id,
                                'name': us.user.first_name if us.user.first_name else '',
                                'birthday': int(time.mktime(datetime.datetime.strptime(f'{us.user.birthday}',
                                                                                       "%Y-%m-%d").timetuple())) if us.user.birthday else 0,
                                'photo': '{0}{1}'.format(settings.BASE_HOST, us.user.photo.url),
                                'total_rating': total_rating.total_popularity,
                                })

        answer = {
            'organizer': event_creator,
            'list_wait_modify': wait_modify,
            'accepted_users': accepted_events,
        }
        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer)
        )
