import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat
from modules.events.models import Events
from modules.users.models import Users
from utils.answer import MISSING_DATA, NOT_FOUND_EVENT, ACCESS_DENIED, SUCCESS_ADD_USER_ON_EVENT, \
    SUCCESS_READ_USER_ON_EVENT, SUCCESS_AWAITING_USER_ON_EVENT

__all__ = ['AddUserMainWindowsAPI', ]

LOGGER = logging.getLogger(__name__)


class AddUserMainWindowsAPI(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Добавление пользователя из события на главном экране
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = request.data.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            events = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NOT_FOUND_EVENT)
            )

        user = Users.objects.get(id=self.request.user.id)
        event = Events.objects.get(id=event_id)

        if events.accept_automatic:
            events.list_users.add(user)
            try:
                access = AccessForEvents.objects.get(user=user,
                                                     event_id=event)
                access.access = AccessForEvents.GOES
                access.save()
            except:
                access = AccessForEvents()
                access.user = user
                access.event = event
                access.access = AccessForEvents.GOES
                access.save()

            # добавление пользователя в чат
            try:
                chat = Chat.objects.get(event__id=event_id)
                users = event.list_users.all()
                creator_event = event.create_event

                if users:
                    for user in users:
                        chat.users.add(Users.objects.get(id=user.id))

                    chat.users.add(Users.objects.get(id=creator_event.id))

                    chat.users.add(Users.objects.get(id=request.user.id))
                else:
                    chat.users.add(Users.objects.get(id=creator_event.id))
                    chat.users.add(Users.objects.get(id=request.user.id))

            except:
                chat = Chat()
                chat.event = event
                users = event.list_users.all()
                creator_event = event.create_event
                chat.save()

                if users:
                    for user in users:
                        chat.users.add(Users.objects.get(id=user.id))

                    chat.users.add(Users.objects.get(id=creator_event.id))

                    chat.users.add(Users.objects.get(id=request.user.id))
                else:
                    chat.users.add(Users.objects.get(id=creator_event.id))
                    chat.users.add(Users.objects.get(id=request.user.id))

            return Response(
                status=status.HTTP_200_OK,
                data=self.get_answer(SUCCESS_ADD_USER_ON_EVENT)
            )

        elif not events.accept_automatic:

            try:
                access = AccessForEvents.objects.get(user=user,
                                                     event_id=event)
                access.access = AccessForEvents.AWAITING_CONFIRMATION
                access.save()
            except:
                access = AccessForEvents()
                access.user = user
                access.event = event
                access.access = AccessForEvents.AWAITING_CONFIRMATION
                access.save()

            return Response(
                status=status.HTTP_200_OK,
                data=self.get_answer(SUCCESS_AWAITING_USER_ON_EVENT)
            )


class DeleteUserMainWindowsAPI(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Добавление или удаление пользователя из события на гл
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = request.data.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            events = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NOT_FOUND_EVENT)
            )

        user = Users.objects.get(id=self.request.user.id)
        event = Events.objects.get(id=event_id)

        # статус "просмотрено" на событие для пользователя
        try:
            access = AccessForEvents.objects.get(user=user,
                                                 event_id=event)
            access.access = AccessForEvents.DOES_NOT_GO
            access.save()
        except:
            access = AccessForEvents()
            access.user = user
            access.event = event
            access.access = AccessForEvents.DOES_NOT_GO
            access.save()

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(SUCCESS_READ_USER_ON_EVENT)
        )
