import logging
from rest_framework.response import Response
from rest_framework import status

from configs import settings
from modules.api.v0.base_api_view import BaseAPIView
from modules.events.models import Events
from modules.ratings.models import UserRatings
from modules.users.models import Users
from utils.answer import MISSING_DATA, EVENT_NOT_FOUND
import time
import datetime

__all__ = ['GetListUsersFromEventsAPI', ]

LOGGER = logging.getLogger(__name__)


class GetListUsersFromEventsAPI(BaseAPIView):
    """
    Получить список участников события
    """

    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = self.request.query_params.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(MISSING_DATA)
            )
        answer = []
        users_from_event = []

        try:
            event_creator = Events.objects.get(id=event_id).create_event
        except:
            LOGGER.debug('')
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=EVENT_NOT_FOUND)
            )

        event_creator = {'id': event_creator.id,
                         'name': event_creator.first_name if event_creator.first_name else '',
                         'birthday': int(time.mktime(datetime.datetime.strptime(f'{event_creator.birthday}',
                                                                                "%Y-%m-%d").timetuple())) if event_creator.birthday else 0,
                         'photo': event_creator.get_photo(),
                         'total_rating': event_creator.get_total_rating(),
                         }

        users = Events.objects.filter(id=event_id).values_list('list_users')
        if users[0][0] is None:
            users_from_event = []
        else:
            for user_id in users:
                try:
                    user = Users.objects.get(id=user_id[0])
                except:
                    return Response(
                        status=status.HTTP_200_OK,
                        data=self.get_answer(data=answer)
                    )

                try:
                    total_rating = UserRatings.objects.get(user=self.request.user.id)
                except:
                    total_rating = UserRatings()
                    total_rating.user = user
                    total_rating.total_popularity = 0.0
                    total_rating.number_events_visited = 0
                    total_rating.save()

                users_from_event.append(
                    {
                        'id': user.id,
                        'name': user.first_name if user.first_name else '',
                        'birthday': int(time.mktime(datetime.datetime.strptime(f'{user.birthday}',
                                                                               "%Y-%m-%d").timetuple())) if user.birthday else 0,
                        'total_rating': total_rating.total_popularity,
                        'photo': '{0}{1}'.format(settings.BASE_HOST, user.photo.url),
                    }
                )

        answer = {
            'organizer': event_creator,
            'users': users_from_event,
        }

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer)
        )
