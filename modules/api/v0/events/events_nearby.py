import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView, ViewAPI
from modules.api.v0.events.list_my_events import get_info_access_event, get_event_for_limited

__all__ = ['EventsNearbyAPI', ]

from modules.events.models import Events
from modules.users.models import Users

from utils.answer import ERROR_COORDINATES
from django.utils import timezone

LOGGER = logging.getLogger(__name__)


class EventsNearbyAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Получение списка событий от местоположения в районе 10км
        :param request:
        :param args:
        :param kwargs:
        :return:a
        """

        coordinate_x = str(self.request.query_params.get('coordinate_x'))
        coordinate_y = str(self.request.query_params.get('coordinate_y'))
        value = 1.1

        today = timezone.now()
        after_tomorrow = timezone.now() + timezone.timedelta(3)

        if not all((coordinate_x, coordinate_y)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ERROR_COORDINATES)
            )

        if ',' in coordinate_x:
            coordinate_x = coordinate_x.replace(',', '.')

        if ',' in coordinate_y:
            coordinate_y = coordinate_y.replace(',', '.')

        try:
            coordinate_y = float(coordinate_y)
            coordinate_x = float(coordinate_x)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ERROR_COORDINATES)
            )

        x1 = coordinate_x + value
        x2 = coordinate_x - value
        y1 = coordinate_y + value
        y2 = coordinate_y - value

        events = Events.objects.filter(coordinate_x__range=(x2, x1),
                                       coordinate_y__range=(y2, y1),
                                       date_event__range=(today, after_tomorrow),
                                       is_active=True).exclude(is_online=True)

        events_online = Events.objects.filter(date_event__range=(today, after_tomorrow),
                                              is_active=True,
                                              is_online=True)

        for ev in events:
            if not AccessForEvents.objects.filter(user=request.user,
                                                  event=ev):
                access_event = AccessForEvents()
                access_event.user = request.user
                access_event.event = ev
                access_event.access = AccessForEvents.NO_READ
                access_event.save()
            else:
                if AccessForEvents.objects.filter(user=request.user,
                                                  event=ev):
                    event = AccessForEvents.objects.get(user=request.user,
                                                        event=ev)
                    if event.access != AccessForEvents.NO_READ:
                        events_online.exclude(id=ev.id)

        for ev_online in events_online:
            if not AccessForEvents.objects.filter(user=request.user,
                                                  event=ev_online):
                access_event = AccessForEvents()
                access_event.user = request.user
                access_event.event = ev_online
                access_event.access = AccessForEvents.NO_READ
                access_event.save()
            else:
                if AccessForEvents.objects.filter(user=request.user,
                                                  event=ev_online):
                    event = AccessForEvents.objects.get(user=request.user,
                                                        event=ev_online)
                    if event.access != AccessForEvents.NO_READ:
                        events_online.exclude(id=ev_online.id)

        user = Users.objects.get(id=request.user.id)

        user.location_by_x = coordinate_x
        user.location_by_y = coordinate_y
        user.save()

        if user.gender == Users.MAN:
            list_access_for_events = AccessForEvents.objects.filter(user=request.user,
                                                                    access=AccessForEvents.NO_READ,
                                                                    event__date_event__range=(today, after_tomorrow),
                                                                    event__coordinate_x__range=(x2, x1),
                                                                    event__coordinate_y__range=(y2, y1),
                                                                    event__search_gender__in=[Users.MAN, Users.ALL],
                                                                    event__is_active=True,
                                                                    )
        elif user.gender == Users.WOMAN:
            list_access_for_events = AccessForEvents.objects.filter(user=request.user,
                                                                    access=AccessForEvents.NO_READ,
                                                                    event__date_event__range=(today, after_tomorrow),
                                                                    event__coordinate_x__range=(x2, x1),
                                                                    event__coordinate_y__range=(y2, y1),
                                                                    event__search_gender__in=[Users.WOMAN, Users.ALL],
                                                                    event__is_active=True,
                                                                    )
        else:
            list_access_for_events = AccessForEvents.objects.filter(user=request.user,
                                                                    access=AccessForEvents.NO_READ,
                                                                    event__date_event__range=(today, after_tomorrow),
                                                                    event__coordinate_x__range=(x2, x1),
                                                                    event__coordinate_y__range=(y2, y1),
                                                                    event__search_gender__in=[Users.WOMAN, Users.MAN,
                                                                                              Users.ALL],
                                                                    event__is_active=True,
                                                                    event__is_online=False,
                                                                    )
        list_event_online = AccessForEvents.objects.filter(user=request.user,
                                                           access=AccessForEvents.NO_READ,
                                                           event__date_event__range=(today, after_tomorrow),
                                                           event__is_online=True,
                                                           event__is_active=True)
        list_access_for_events = list(list_access_for_events)
        for i in list_event_online:
            list_access_for_events.append(i)

        answer_api = get_info_access_event(list_access_for_events)

        if len(list_access_for_events) == 0:
            LOGGER.info('No data to send or blank answer')
            return Response(
                status=status.HTTP_200_OK,
                data=self.get_answer(data=[])
            )

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )


class LimitedAPI(ViewAPI):

    def get(self, request, *args, **kwargs):
        """
        Получение списка событий для неавторизованного
        :param request:
        :param args:
        :param kwargs:
        :return:a
        """

        coordinate_x = str(self.request.query_params.get('coordinate_x'))
        coordinate_y = str(self.request.query_params.get('coordinate_y'))
        value = 1.1

        today = timezone.now()
        after_tomorrow = timezone.now() + timezone.timedelta(3)

        if not all((coordinate_x, coordinate_y)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ERROR_COORDINATES)
            )

        if ',' in coordinate_x:
            coordinate_x = coordinate_x.replace(',', '.')

        if ',' in coordinate_y:
            coordinate_y = coordinate_y.replace(',', '.')

        try:
            coordinate_y = float(coordinate_y)
            coordinate_x = float(coordinate_x)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ERROR_COORDINATES)
            )

        x1 = coordinate_x + value
        x2 = coordinate_x - value
        y1 = coordinate_y + value
        y2 = coordinate_y - value

        events = Events.objects.filter(coordinate_x__range=(x2, x1),
                                       coordinate_y__range=(y2, y1),
                                       date_event__range=(today, after_tomorrow),
                                       is_active=True).exclude(is_online=True)

        events_online = Events.objects.filter(date_event__range=(today, after_tomorrow),
                                              is_active=True,
                                              is_online=True)

        all_events = []

        for i in events:
            all_events.append(i)

        for i in events_online:
            all_events.append(i)

        answer_api = get_event_for_limited(all_events)

        if len(all_events) == 0:
            LOGGER.info('No data to send or blank answer')
            return Response(
                status=status.HTTP_200_OK,
                data=self.get_answer(data=[])
            )

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )
