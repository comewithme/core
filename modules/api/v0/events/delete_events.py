import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.events.models import Events
from utils.answer import MISSING_DATA, SUCCESS_DELETE_EVENT, NOT_FOUND_EVENT, ACCESS_DENIED

__all__ = ['DeleteEventsAPI', ]

LOGGER = logging.getLogger(__name__)


class DeleteEventsAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Удаление события
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        event_id = self.request.query_params.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(MISSING_DATA)
            )
        try:
            events = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(NOT_FOUND_EVENT)
            )

        if events.create_event != request.user:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ACCESS_DENIED)
            )
        else:
            Events.objects.filter(id=event_id,
                                  create_event=self.request.user).delete()

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(SUCCESS_DELETE_EVENT)
        )
