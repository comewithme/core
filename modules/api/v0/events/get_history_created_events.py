import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from django.utils import timezone
from modules.api.v0.events.list_my_events import get_info_access_event

__all__ = ['HistoryCreatedEventsAPI', ]

from utils.answer import EVENT_NOT_FOUND

LOGGER = logging.getLogger(__name__)


class HistoryCreatedEventsAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        история событий которые я создал
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        now = timezone.now()
        list_accepted_events = AccessForEvents.objects.filter(user=self.request.user,
                                                              event__date_event__lt=now,
                                                              event__is_active=True,
                                                              event__create_event=self.request.user,
                                                              access__in=[AccessForEvents.CREATED,
                                                                          ])

        if not list_accepted_events:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data=self.get_answer(error=EVENT_NOT_FOUND)
            )

        answer_api = get_info_access_event(list_accepted_events)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )
