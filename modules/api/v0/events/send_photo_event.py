import logging
from rest_framework.response import Response
from rest_framework import status
from django.core.files.storage import default_storage

from configs import BASE_HOST
from modules.api.v0.base_api_view import BaseAPIView
from utils.answer import MISSING_DATA, SUCCESS_CREATE_EVENT, SUCCESS_SAVE_IN_CACHE, ERROR_SAVE_IN_CACHE
from django.core.cache import cache
import urllib.parse

__all__ = ['SendPhotoAPI', ]

LOGGER = logging.getLogger(__name__)


class SendPhotoAPI(BaseAPIView):
    """
    Создание события
    """

    def post(self, request, *args, **kwargs):
        """
        :param request:
        """
        photo_events = request.data.get('photo_event')

        if not all((photo_events,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            cache.set(request.user.id, photo_events, 60)
            file_name = default_storage.save(photo_events.name, photo_events)
            params = f'media/{file_name}'
            photo_url = urllib.parse.urljoin(BASE_HOST, params)
        except Exception as ex:
            LOGGER.info(f'Can not save in cache for {request.user.id} and value={photo_events} with error {ex}')
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ERROR_SAVE_IN_CACHE)
            )

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=photo_url)
        )
