import logging
from datetime import datetime

from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from django.utils import timezone

__all__ = ['SearchEventAPI', ]

from modules.api.v0.events.list_my_events import get_info_access_event
from modules.events.models import Events, Category
from modules.ratings.models import UserRatings
from modules.users.models import Users

from utils.answer import MISSING_DATA, ERROR_COORDINATES, NO_DATA

LOGGER = logging.getLogger(__name__)


class SearchEventAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        поиск событий - экран поиска
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # обязательные
        distance = self.request.query_params.get('distance')
        coordinate_x = self.request.query_params.get('coordinate_x')
        coordinate_y = self.request.query_params.get('coordinate_y')

        price = self.request.query_params.get('price')
        tags = self.request.query_params.get('tags')
        date_from = self.request.query_params.get('date_from')
        date_to = self.request.query_params.get('date_to')

        now = timezone.now()

        if not all((distance, coordinate_x, coordinate_y)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        if ',' in coordinate_x:
            coordinate_x = coordinate_x.replace(',', '.')

        if ',' in coordinate_y:
            coordinate_y = coordinate_y.replace(',', '.')

        try:
            coordinate_y = float(coordinate_y)
            coordinate_x = float(coordinate_x)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=ERROR_COORDINATES)
            )

        # TODO change on category
        # if tags:
        #     tags = str(tags).replace(' ', '')
        #     list_tags_id = tags.split(',')
        #     tags = Tags.objects.filter(id__in=list_tags_id)
        # else:
        #     tags = None

        if not date_from:
            date_from = now
        else:
            date_from = datetime.fromtimestamp(int(date_from)).strftime("%Y-%m-%d %H:%M:%S")

        if not date_to:
            date_to = None
        else:
            date_to = datetime.fromtimestamp(int(date_to)).strftime("%Y-%m-%d %H:%M:%S")

        # 5 10 15 20
        if int(distance) == 5:
            distance = 0.5
        elif int(distance) == 10:
            distance = 1.0
        elif int(distance) == 15:
            distance = 1.5
        elif int(distance) == 20:
            distance = 2.0
        elif distance is None:
            distance = 1.0

        if price is None:
            price = 0.0

        x1 = coordinate_x + float(distance)
        x2 = coordinate_x - float(distance)
        y1 = coordinate_y + float(distance)
        y2 = coordinate_y - float(distance)

        if date_to is None:
            events = Events.objects.filter(
                coordinate_x__range=(x2, x1),
                coordinate_y__range=(y2, y1),
                date_event__gte=date_from,
                price__lte=price,
                tags__in=tags,
            ).values_list('id')
        else:
            events = Events.objects.filter(
                coordinate_x__range=(x2, x1),
                coordinate_y__range=(y2, y1),
                tags__in=tags,
                date_event__range=[date_from, date_to],
                price__lte=price,
            ).values_list('id')

        answer_api = []

        for ev in events:
            event = Events.objects.get(id=ev[0])

            try:
                user_photo = event.create_event.get_photo()
            except:
                user_photo = None

            # TODO change on category

            # tags = []
            #
            # try:
            #     tags_event_id = Events.objects.filter(id=ev[0]).values_list('tags')
            #     for tag_id in tags_event_id:
            #         tag = Tags.objects.get(id=tag_id[0])
            #         tags.append({
            #             'id': tag.id,
            #             'name': tag.name
            #         })
            # except:
            #     tags = []

            try:
                total_rating = UserRatings.objects.get(id=event.create_event.id)
                total_rating = total_rating.total_popularity
            except:
                total_rating = 0.0

            creator_event = [{
                'id': event.create_event.id,
                'name': event.create_event.first_name,
                'birthday': event.create_event.birthday,
                'photo': user_photo,
                'phone': event.create_event.phone,
                'total_rating': total_rating,
            }]

            try:
                event_photo = event.get_photo_event()
            except:
                event_photo = None

            answer_api.append({
                'id': event.id,
                'name': event.name,
                'description': event.description,
                'address': event.address,
                'tags': tags,
                'color': event.color,
                'price': event.price,
                'max_count_users': event.max_count_users,
                'create_event': creator_event,
                'photo_event': event_photo,
                'date_event': event.date_event,
            })

        if len(events) == 0:
            LOGGER.info('No data to send or blank answer')
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=NO_DATA)
            )

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )
