# TODO написать тест для редактирования события
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from django.test import TestCase
from datetime import datetime
from modules.api.v0.events.change_events import ChangeEventsAPI
from modules.api.v0.test_utils import get_test_objects
from modules.events.models import Events, Tags
from modules.users.models import Users
from modules.api.v0.events.delete_events import DeleteEventsAPI

class TestEvent(TestCase):
    def setUp(self) -> None:
        user, _ = Users.objects.get_or_create(phone='89271111111')
        user2, _ = Users.objects.get_or_create(phone='89271211111', email="xdtfyc.dc@mail.dc")
        tegs, _ = Tags.objects.get_or_create(name="#hello")
        event, _ = Events.objects.get_or_create(
            name="Вызов дьявола1",
            description="писи каки",
            coordinate_x="54.7410",
            coordinate_y='55.9889',
            price=1000,
            max_count_users="33",
            create_event=user,
            search_gender=1,
            date_event=datetime.fromtimestamp(1610523095).strftime("%Y-%m-%d %H:%M:%S"),
            date_create_event=datetime.fromtimestamp(1610523095).strftime("%Y-%m-%d %H:%M:%S")

        )
        event.list_users.add(user)
        event.tags.add(tegs)
        event2, _ = Events.objects.get_or_create(
            name="Вызов дьявола",
            description="писи каки",
            coordinate_x="54.7410",
            coordinate_y='55.9889',
            price=1000,
            max_count_users="33",
            create_event=user2,
            search_gender=1,
            date_event=datetime.fromtimestamp(1610523095).strftime("%Y-%m-%d %H:%M:%S"),
            date_create_event=datetime.fromtimestamp(1610523095).strftime("%Y-%m-%d %H:%M:%S")

        )
        event2.tags.add(tegs)
        event2.list_users.add(user2)

    def test_post_event(self):
        """
        Редактирование событий
        :return:
        """
        obj = get_test_objects()
        factory = APIRequestFactory()
        event_id = Events.objects.get(name="Вызов дьявола1")
        request = factory.post(
            reverse('change-event'),
            HTTP_AUTHORIZATION="JWT " + obj.get('tokens').get('access').decode("utf-8"),
            data={
                "event_id": event_id.id,

            }
        )
        view = ChangeEventsAPI.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get('detail'))
        self.assertEqual(response.data.get('status'), 'ok')

    def test_post_event_error(self):
        """
        отправка пустых данных
        :return:
        """
        obj = get_test_objects()
        factory = APIRequestFactory()
        request = factory.post(
            reverse('change-event'),
            HTTP_AUTHORIZATION="JWT " + obj.get('tokens').get('access').decode("utf-8"),
            data={
            }
        )
        view = ChangeEventsAPI.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 400)
        self.assertIsNotNone(response.data.get('detail'))
        self.assertEqual(response.data.get('status'), 'ok')

    def test_post_event_user(self):
        """
            Отправка чужоого event на редактирование
            :return:
            """
        obj = get_test_objects()
        factory = APIRequestFactory()
        request = factory.post(
            reverse('change-event'),
            HTTP_AUTHORIZATION="JWT " + obj.get('tokens').get('access').decode("utf-8"),
            data={
                "event_id": 2,

            }
        )
        view = ChangeEventsAPI.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 400)
        self.assertIsNotNone(response.data.get('detail'))
        self.assertEqual(response.data.get('status'), 'ok')

    def test_delete_event_user(self):
        """
            Удаление event
            :return:
            """
        obj = get_test_objects()
        factory = APIRequestFactory()
        event_id = Events.objects.get(name="Вызов дьявола")
        request = factory.get(
            '{0}{1}'.format(reverse('delete-event'),'?event_id={0}'.format(event_id.id)),
            HTTP_AUTHORIZATION="JWT " + obj.get('tokens').get('access').decode("utf-8"),
        )
        view = DeleteEventsAPI.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 400)
        self.assertIsNotNone(response.data.get('detail'))
        self.assertEqual(response.data.get('status'), 'ok')