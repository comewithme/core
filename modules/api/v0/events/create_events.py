import logging

from django.core.files.base import ContentFile
from rest_framework.response import Response
from rest_framework import status
from base64 import b64decode

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat
from modules.events.models import Events, Category, ColorGradient
from utils.answer import MISSING_DATA, SUCCESS_CREATE_EVENT, INCORRECT_SEARCH_GENDER, \
    INCORRECT_COLOR_GRADIENT, COLOR_GRADIENT_NOT_FOUND, INCORRECT_MAX_COUNT_USERS
from datetime import datetime
import uuid

__all__ = ['CreateEventsAPI', ]

LOGGER = logging.getLogger(__name__)


class CreateEventsAPI(BaseAPIView):
    """
    Создание события
    """

    def post(self, request, *args, **kwargs):
        """
        :param request:
        """
        # обязательные поля
        name = request.data.get('name')
        description = request.data.get('description')
        coordinate_x = request.data.get('coordinate_x')
        coordinate_y = request.data.get('coordinate_y')
        max_count_users = request.data.get('max_count_users')
        address = request.data.get('address')
        date_event = request.data.get('date_event')
        url_site = request.data.get('url_site')
        is_online = request.data.get('is_online')

        category = request.data.get('category')
        accept_automatic = request.data.get('accept_automatic')
        color_id = request.data.get('color_id')
        price = request.data.get('price')
        search_gender = request.data.get('search_gender')
        photo_events = request.data.get('photo_event')

        if not all((name, address, description, coordinate_x, coordinate_y, max_count_users, date_event)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        if date_event:
            date_event = datetime.fromtimestamp(int(date_event)).strftime("%Y-%m-%d %H:%M:%S")

        try:
            if search_gender:
                search_gender = int(search_gender)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=INCORRECT_SEARCH_GENDER)
            )

        if not is_online:
            is_online = False

        if search_gender == 1:
            search_gender = Events.MAN
        elif search_gender == 2:
            search_gender = Events.WOMAN
        elif search_gender == 0 or search_gender is None:
            search_gender = Events.ALL
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=f'{INCORRECT_SEARCH_GENDER} со значением: {search_gender}')
            )

        try:
            max_count_users = int(max_count_users)

            if max_count_users > 500:
                return Response(
                    status=status.HTTP_200_OK,
                    data=self.get_answer(error=f'{INCORRECT_MAX_COUNT_USERS} со значением: {max_count_users}')
                )
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=f'{INCORRECT_MAX_COUNT_USERS} со значением: {max_count_users}')
            )

        if color_id:
            try:
                color_id = int(color_id)
            except:
                return Response(
                    status=status.HTTP_200_OK,
                    data=self.get_answer(error=f'{INCORRECT_COLOR_GRADIENT} со значением: {color_id}')
                )

            try:
                color_id = ColorGradient.objects.get(id=color_id)
            except:
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data=self.get_answer(error=f'{COLOR_GRADIENT_NOT_FOUND}')
                )
        else:
            color_id = None

        if accept_automatic:
            accept_automatic = True
        elif not accept_automatic:
            accept_automatic = False
        try:
            category = Category.objects.get(id=category)
        except:
            category = None

        create_event = Events()
        create_event.name = name
        create_event.description = description
        create_event.address = address
        create_event.color = color_id
        create_event.coordinate_x = coordinate_x
        create_event.coordinate_y = coordinate_y
        create_event.price = price
        create_event.category = category
        create_event.url_site = url_site
        create_event.is_online = is_online
        create_event.accept_automatic = accept_automatic
        create_event.search_gender = search_gender
        create_event.max_count_users = max_count_users
        create_event.create_event = request.user

        if photo_events:
            image_data = b64decode(photo_events)
            image_name = str(uuid.uuid4()) + ".jpg"
            create_event.photo_event = ContentFile(image_data, image_name)

        create_event.date_event = date_event
        create_event.save()
        LOGGER.info('Success create event')

        AccessForEvents.objects.create(user=request.user,
                                       event=create_event,
                                       access=AccessForEvents.CREATED)

        if not Chat.objects.filter(event=create_event):
            chat = Chat.objects.create(event=create_event)
            # TODO Change save photo
            chat.save()
            chat.users.add(request.user)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(SUCCESS_CREATE_EVENT)
        )
