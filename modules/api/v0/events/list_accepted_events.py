import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView

from django.utils import timezone

__all__ = ['ListAcceptedEventsAPI', ]

from modules.api.v0.events.list_my_events import get_info_access_event

LOGGER = logging.getLogger(__name__)


class ListAcceptedEventsAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Список принятых событий(куда иду, жду ожидания, отклонены)
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        now = timezone.now()

        list_accepted_events = AccessForEvents.objects.filter(user=self.request.user,
                                                              event__date_event__gte=now,
                                                              event__is_active=True,
                                                              access__in=[AccessForEvents.GOES,
                                                                          AccessForEvents.AWAITING_CONFIRMATION,
                                                                          AccessForEvents.REJECTED_CREATOR])

        answer_api = get_info_access_event(list_accepted_events)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )
