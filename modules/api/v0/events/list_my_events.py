import logging
from rest_framework.response import Response
from rest_framework import status

from modules.access_the_events.models import AccessForEvents
from modules.api.v0.base_api_view import BaseAPIView
from django.utils import timezone

import time
import datetime

__all__ = ['ListMyEventsAPI', ]

from modules.chat.models import Chat
from modules.events.models import Events, Category
from modules.ratings.models import UserRatings
from modules.users.models import Users

LOGGER = logging.getLogger(__name__)


def get_info_access_event(events):
    answer = []

    for accept_event in events:
        ac_event = AccessForEvents.objects.get(id=accept_event.id)
        event = Events.objects.get(id=ac_event.event.id)
        creator = Users.objects.get(id=event.create_event.id)

        try:
            user_photo = creator.get_photo()
        except:
            user_photo = None
        try:
            total_rating = UserRatings.objects.get(id=creator.id)
            total_rating = total_rating.total_popularity
        except:
            total_rating = 0.0

        try:
            chat = Chat.objects.get(event=event)
            users = event.list_users.all()
            creator_event = event.create_event

            if users:
                for user in users:
                    chat.users.add(Users.objects.get(id=user.id))

                chat.users.add(Users.objects.get(id=creator_event.id))
            else:
                chat.users.add(Users.objects.get(id=creator_event.id))

        except:
            chat = Chat()
            chat.event = event
            users = event.list_users.all()
            creator_event = event.create_event
            chat.save()

            if users:
                for user in users:
                    chat.users.add(Users.objects.get(id=user.id))
                chat.users.add(Users.objects.get(id=creator_event.id))
            else:
                chat.users.add(Users.objects.get(id=creator_event.id))

        creator_event = {
            'id': creator.id,
            'name': creator.first_name if creator.first_name else '',
            'birthday': int(time.mktime(datetime.datetime.strptime(f'{creator.birthday}',
                                                                   "%Y-%m-%d").timetuple())) if creator.birthday else 0,
            'photo': user_photo,
            'phone': creator.phone if creator.phone else '',
            'vk': creator.vk if creator.vk else '',
            'instagram': creator.instagram if creator.instagram else '',
            'telegram': creator.telegram if creator.telegram else '',
            'total_rating': total_rating,
        }

        if event.color is None:
            color = None
        else:
            color = {
                'start_color': event.color.start_color,
                'end_color': event.color.end_color,
                'color_text': event.color.color_text,
                'angle': event.color.angle,
            }

        if event.date_event:
            data = event.date_event
            seconds = int(data.timestamp())

        try:
            cat = Category.objects.get(id=event.category.id)
            category = {
                "id": cat.id,
                "name": cat.name
            }
        except:
            category = None

        number_participants = len(AccessForEvents.objects.filter(event=event,
                                                                 access=AccessForEvents.GOES))

        answer.append({
            'id': event.id,
            "chat_id": chat.id,
            'name': event.name,
            'description': event.description if event.description else '',
            'address': event.address if event.address else '',
            'color': color,
            'url_site': event.url_site if event.url_site else '',
            'price': event.price if event.price else 0.0,
            'is_online': event.is_online,
            'max_count_users': event.max_count_users,
            'create_event': creator_event,
            'category': category,
            'photo_event': event.get_photo_event(),
            'date_event': seconds,
            'access': ac_event.access,
            'number_participants': number_participants if number_participants else 0
        })

    return answer


def get_event_for_limited(events):
    answer = []

    for event in events:
        event = event
        creator = Users.objects.get(id=event.create_event.id)

        try:
            user_photo = creator.get_photo()
        except:
            user_photo = None
        try:
            total_rating = UserRatings.objects.get(id=creator.id)
            total_rating = total_rating.total_popularity
        except:
            total_rating = 0.0

        creator_event = {
            'id': creator.id,
            'name': creator.first_name if creator.first_name else '',
            'birthday': int(time.mktime(datetime.datetime.strptime(f'{creator.birthday}',
                                                                   "%Y-%m-%d").timetuple())) if creator.birthday else 0,
            'photo': user_photo,
            'phone': creator.phone if creator.phone else '',
            'vk': creator.vk if creator.vk else '',
            'instagram': creator.instagram if creator.instagram else '',
            'telegram': creator.telegram if creator.telegram else '',
            'total_rating': total_rating,
        }

        if event.color is None:
            color = None
        else:
            color = {
                'start_color': event.color.start_color,
                'end_color': event.color.end_color,
                'color_text': event.color.color_text,
                'angle': event.color.angle,
            }

        if event.date_event:
            data = event.date_event
            seconds = int(data.timestamp())

        try:
            cat = Category.objects.get(id=event.category.id)
            category = {
                "id": cat.id,
                "name": cat.name
            }
        except:
            category = None

        number_participants = len(AccessForEvents.objects.filter(event=event,
                                                                 access=AccessForEvents.GOES))

        answer.append({
            'id': event.id,
            'name': event.name,
            'description': event.description if event.description else '',
            'address': event.address if event.address else '',
            'color': color,
            'url_site': event.url_site if event.url_site else '',
            'price': event.price if event.price else 0.0,
            'is_online': event.is_online,
            'max_count_users': event.max_count_users,
            'create_event': creator_event,
            'category': category,
            'photo_event': event.get_photo_event(),
            'date_event': seconds,
            'access': 2,
            'number_participants': number_participants if number_participants else 0
        })

    return answer


class ListMyEventsAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Список моих созданных событий
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        now = timezone.now()

        list_accepted_events = AccessForEvents.objects.filter(user=self.request.user,
                                                              event__date_event__gte=now,
                                                              event__is_active=True,
                                                              access=AccessForEvents.CREATED)
        answer_api = get_info_access_event(list_accepted_events)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer_api)
        )
