from django.urls import path

from modules.api.v0.events.add_or_delete_user_main_windows import AddUserMainWindowsAPI, DeleteUserMainWindowsAPI
from modules.api.v0.events.add_user_on_event import AddUserOnEventAPI
from modules.api.v0.events.change_events import ChangeEventsAPI
from modules.api.v0.events.create_events import CreateEventsAPI
from modules.api.v0.events.delete_events import DeleteEventsAPI
from modules.api.v0.events.delete_user_from_event import DeleteUserFromEventAPI
from modules.api.v0.events.events_nearby import EventsNearbyAPI, LimitedAPI
from modules.api.v0.events.get_history_accepted_events import HistoryAcceptedEventsAPI
from modules.api.v0.events.get_history_created_events import HistoryCreatedEventsAPI
from modules.api.v0.events.get_list_users_from_events import GetListUsersFromEventsAPI
from modules.api.v0.events.get_user_accept_and_modify import ListAcceptUserAndWaitMModifyAPI
from modules.api.v0.events.list_accepted_events import ListAcceptedEventsAPI
from modules.api.v0.events.list_my_events import ListMyEventsAPI
from modules.api.v0.events.refuse_participate import RefuseParticipateAPI
from modules.api.v0.events.search_events import SearchEventAPI
from modules.api.v0.events.send_photo_event import SendPhotoAPI

urlpatterns = [
    path('send_photo/', SendPhotoAPI.as_view(), name='send-photo'),
    path('create/', CreateEventsAPI.as_view(), name='create-event'),
    path('delete/', DeleteEventsAPI.as_view(), name='delete-event'),
    path('change/', ChangeEventsAPI.as_view(), name='change-event'),

    # добавить юзера на событие
    path('add_user_on_event/', AddUserOnEventAPI.as_view(), name='add-user-on-event'),
    # удаление пользователя из события
    path('delete_user_from_event/', DeleteUserFromEventAPI.as_view(), name='delete-user-from-event'),
    # отказаться от участия
    path('refuse_participate/', RefuseParticipateAPI.as_view(), name='refuse_participate'),

    # список принятых событий(экран "Куда я иду")
    path('list_accepted_events/', ListAcceptedEventsAPI.as_view(), name='list-accepted-event'),

    # список моих созданных событий
    path('list_my_events/', ListMyEventsAPI.as_view(), name='list-accepted-event'),

    # список близлежайших событий для главного экрана (свайп экран)
    path('events_nearby/', EventsNearbyAPI.as_view(), name='events-nearby'),
    # главный экран для неавторизованного
    path('limited/', LimitedAPI.as_view(), name='events-limited'),

    # добавить или убрать пользователя на событии в главном экране
    path('accept_event/', AddUserMainWindowsAPI.as_view(), name='accept_event'),
    path('refuse_event/', DeleteUserMainWindowsAPI.as_view(), name='refuse_event'),

    # получить список участников события
    path('get_list_users_from_events/', GetListUsersFromEventsAPI.as_view(), name='get_list_users_from_events'),

    # история - куда ходил
    path('get_history_accepted_events/', HistoryAcceptedEventsAPI.as_view(), name='get_history_accepted_events'),

    # история - события, которые я сам создал
    path('get_history_created_events/', HistoryCreatedEventsAPI.as_view(), name='get_history_created_events'),

    # для списка участников подтверждённых и ожидают подтверждения
    path('get_user_accept_and_wait_modify/', ListAcceptUserAndWaitMModifyAPI.as_view(),
         name='get_user_accept_and_wait_modify'),

    # поиск событий - экран поиск
    path('search_events/', SearchEventAPI.as_view(),
         name='search_events'),
]
