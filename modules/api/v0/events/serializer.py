from rest_framework import serializers

from modules.access_the_events.models import AccessForEvents
from modules.events.models import Events, Category
from modules.users.serializer import UsersSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id',
                  'name', ]


class EventsSerializer(serializers.ModelSerializer):
    create_event = UsersSerializer()
    tags = CategorySerializer(many=True)

    class Meta:
        model = Events
        fields = ['id',
                  'name',
                  'description',
                  'address',
                  'tags',
                  'price',
                  'max_count_users',
                  'create_event',
                  'search_gender',
                  'photo_event',
                  'date_event',
                  'date_create_event',
                  ]


class AccessForEventsSerializer(serializers.ModelSerializer):
    event = EventsSerializer()

    class Meta:
        model = AccessForEvents
        fields = ['id',
                  'event',
                  'access'
                  ]
