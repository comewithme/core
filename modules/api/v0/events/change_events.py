import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.events.models import Events, Category
from utils.answer import MISSING_DATA, ACCESS_DENIED, NOT_FOUND_EVENT, SUCCESS_CHANGE_EVENT
from datetime import datetime

__all__ = ['ChangeEventsAPI', ]

LOGGER = logging.getLogger(__name__)


class ChangeEventsAPI(BaseAPIView):

    def patch(self, request, *args, **kwargs):
        """
        Редактирование события
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # обязательные поля
        event_id = request.data.get('event_id')

        name = request.data.get('name')
        description = request.data.get('description')
        coordinate_x = request.data.get('coordinate_x')
        coordinate_y = request.data.get('coordinate_y')
        max_count_users = request.data.get('max_count_users')
        address = request.data.get('address')
        date_event = request.data.get('date_event')
        category = request.data.get('category')
        accept_automatic = request.data.get('accept_automatic')
        color = request.data.get('color')
        is_online = request.data.get('is_online')
        price = request.data.get('price')
        search_gender = request.data.get('search_gender')
        photo_events = request.data.get('photo_event')
        url_site = request.data.get('url_site')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        if date_event:
            date_event = datetime.fromtimestamp(int(date_event)).strftime("%Y-%m-%d %H:%M:%S")

        if not is_online:
            is_online = False

        try:
            events = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(NOT_FOUND_EVENT)
            )

        try:
            if search_gender == 1:
                search_gender = Events.MAN
            elif search_gender == 2:
                search_gender = Events.WOMAN
            else:
                search_gender = Events.ALL
        except Exception as ex:
            LOGGER.error(f"Can not change gender on {events}: {ex}")

        try:
            category = Category.objects.get(id=category)
        except:
            category = None

        if events.create_event != request.user:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(ACCESS_DENIED)
            )
        else:
            event = Events.objects.get(id=event_id,
                                       create_event=self.request.user)
            if name:
                event.name = name
            if description:
                event.description = description
            if address:
                event.address = address
            if color:
                event.color = color
            if coordinate_x:
                event.coordinate_x = coordinate_x
            if coordinate_y:
                event.coordinate_y = coordinate_y
            if price:
                event.price = price
            if category:
                event.category = category
            if accept_automatic:
                event.accept_automatic = accept_automatic
            if search_gender:
                event.search_gender = search_gender
            if max_count_users:
                event.max_count_users = max_count_users
            if photo_events:
                event.photo_event = photo_events
            if date_event:
                event.date_event = date_event

            event.url_site = url_site if url_site else ''
            event.is_online = is_online

            event.save()
            LOGGER.info('Success change event ', event)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_CHANGE_EVENT)
        )
