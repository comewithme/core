from django.urls import path

from modules.api.v0.category.list_category import ListCategoryApi

urlpatterns = [
    path('list_category/', ListCategoryApi.as_view(), name='list-category'),
]
