import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import ViewAPI
from modules.api.v0.events.serializer import CategorySerializer
from modules.events.models import Category

__all__ = ['ListCategoryApi', ]

from utils.answer import NO_DATA

LOGGER = logging.getLogger(__name__)


class ListCategoryApi(ViewAPI):

    def get(self, request, *args, **kwargs):
        """
        Список категорий
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        category = Category.objects.all()
        if category is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(NO_DATA)
            )

        serializer = CategorySerializer(category, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=serializer.data)
        )
