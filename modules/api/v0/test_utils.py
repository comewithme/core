

from modules.api.v0.jwt_auth.utils import get_jwt_token
from modules.users.models import Users
from utils.utils import decode_jwt_token

__all__ = ['get_test_objects']


def get_test_objects():
    """
    Метод для получения объектов для тестов таких как:
    1) Токен
    :return:
    """
    user, _ = Users.objects.get_or_create(phone='89271111111')  # Основной пользователь для тестов
    output = get_jwt_token(user)
    output['user'] = user
    return output
