import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import ViewAPI
from modules.core.models.city import City

__all__ = ['CityApi', ]

from modules.events.serializer.color_gradient_serializer import CitySerializer

from utils.answer import NO_DATA

LOGGER = logging.getLogger(__name__)


class CityApi(ViewAPI):

    def get(self, request, *args, **kwargs):
        """Список всех городов."""

        citys = City.objects.all()
        if citys is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(NO_DATA)
            )

        serializer = CitySerializer(citys, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=serializer.data)
        )
