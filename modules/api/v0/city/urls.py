from django.urls import path

from modules.api.v0.city.citys import CityApi

urlpatterns = [
    path('', CityApi.as_view(), name='citys'),
]
