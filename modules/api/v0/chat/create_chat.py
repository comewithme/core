import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.api.v0.chat.serializer import ChatSerializer
from modules.chat.models import Chat
from modules.users.models import Users
from utils.answer import USER_NOT_FOUND, NO_DATA

__all__ = ['CreateChatAPI', ]

LOGGER = logging.getLogger(__name__)


class CreateChatAPI(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Создать чат
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        chats = Chat.objects.filter(users__id=request.user.id)
