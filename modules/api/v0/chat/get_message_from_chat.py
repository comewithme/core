import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat, Message
from utils.answer import MISSING_DATA, CHAT_NOT_FOUND
import time
import datetime

__all__ = ['GetMessageFromChatAPI', ]

LOGGER = logging.getLogger(__name__)


class GetMessageFromChatAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Получить последние сообщения из чата
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        chat_id = self.request.query_params.get('chat_id')
        personal_chat = self.request.query_params.get('personal_chat')

        if not personal_chat or personal_chat == "False" or personal_chat == "false" or personal_chat == "0" or personal_chat == 0:
            try:
                chats = Chat.objects.get(id=chat_id)
            except Chat.DoesNotExist:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=self.get_answer(data=CHAT_NOT_FOUND)
                )

            messages_from_chat = Message.objects.filter(to_chat=chats)[:30]
            creator_event = chats.event.create_event

            messages = []

            for mes in messages_from_chat:
                if mes.sender == creator_event:
                    role = 'Организатор'
                else:
                    role = 'Участник'

                messages.append({
                    "user_id": mes.sender.id,
                    "user": f"{mes.sender}",
                    "message": f"{mes.message}",
                    "message_id": mes.id,
                    "user_photo": f"{mes.sender.get_photo()}",
                    "user_role": role,
                    "user_age": int(time.mktime(datetime.datetime.strptime(f'{mes.sender.birthday}',
                                                                           "%Y-%m-%d").timetuple())) if mes.sender.birthday else 0,
                    "date_created": int(mes.date_created.timestamp()) if mes.date_created else 0
                })

            answer = {
                'name_chat': f"{chats}",
                'photo_chat': f"{chats.event.get_photo_event()}",
                'messages': messages
            }

            return Response(
                status=status.HTTP_200_OK,
                data=self.get_answer(data=answer)
            )

        elif personal_chat or personal_chat == 'True' or personal_chat == "true" or personal_chat == "1" or personal_chat == 1:
            try:
                chats = Chat.objects.get(id=chat_id)
            except Chat.DoesNotExist:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=self.get_answer(data=CHAT_NOT_FOUND)
                )

            messages_from_chat = Message.objects.filter(to_chat=chats)[:30]

            if not messages_from_chat:
                return Response(
                    status=status.HTTP_200_OK,
                    data=self.get_answer(data=[])
                )

            messages = []

            if chats.personal_chat:
                for mes in messages_from_chat:
                    messages.append({
                        "user_id": mes.sender.id,
                        "user": f"{mes.sender}",
                        "message": f"{mes.message}",
                        "message_id": mes.id,
                        "user_photo": f"{mes.sender.get_photo()}",
                        "user_role": '',
                        "user_age": int(time.mktime(datetime.datetime.strptime(f'{mes.sender.birthday}',
                                                                               "%Y-%m-%d").timetuple())) if mes.sender.birthday else 0,
                        "date_created": int(mes.date_created.timestamp()) if mes.date_created else 0
                    })
            answer = {
                'name_chat': f"{chats.users.exclude(id=request.user.id)[0]}",
                'photo_chat': f"{chats.users.exclude(id=request.user.id)[0].get_photo()}",
                'messages': messages
            }

            return Response(
                status=status.HTTP_200_OK,
                data=self.get_answer(data=answer)
            )

        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )
