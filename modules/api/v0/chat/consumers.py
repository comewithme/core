import json
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from modules.chat.models import Chat, Message
import time
import datetime
from asgiref.sync import sync_to_async

from modules.users.models import Users


@database_sync_to_async
def load_chat(chat_id):
    return Chat.objects.prefetch_related('users').get(id=chat_id)


@database_sync_to_async
def create_message(sender, text, to_chat):
    message = Message.objects.create(sender=sender, message=text, to_chat=to_chat)
    return sender


@database_sync_to_async
def user_info(sender_id):
    user = Users.objects.get(id=sender_id)
    return {
        "user": f"{user}",
        "user_photo": user.get_photo(),
        "user_age": int(time.mktime(datetime.datetime.strptime(f'{user.birthday}',
                                                               "%Y-%m-%d").timetuple())) if user.birthday else 0
    }


class ChatConsumer(AsyncWebsocketConsumer):
    """
    Консьюмер асинхронный, но не факт что тебе здесь такой нужен, возможно стоит переделать на синхронный
    """

    async def connect(self):
        self.chat_id = self.scope['url_route']['kwargs']['chat_id']
        self.room_group_name = 'chat_%s' % self.chat_id

        await self.init_chat()

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def init_chat(self):
        self.chat = await self.get_chat()
        # Проверяем участник ли он этого чата, если конечно я правильно понял для чего тебе это поле
        if self.scope['user'] not in self.chat.users.all():
            await self.close()

    async def get_chat(self):
        chat_id = self.scope['url_route']['kwargs']['chat_id']
        chat = await load_chat(chat_id)
        return chat

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        message_id = text_data_json['id']

        self.sender = await create_message(self.scope['user'], message, self.chat)
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'id': message_id,
                'sender_id': self.sender.id
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        message_id = event['id']
        sender_id = event['sender_id']

        info_user = await user_info(sender_id)

        await self.send(text_data=json.dumps({
            'message': message,
            'message_id': int(message_id),
            "user_id": sender_id,
            "user": info_user["user"],
            "user_photo": info_user["user_photo"],
            "user_age": info_user["user_age"],
        }))
