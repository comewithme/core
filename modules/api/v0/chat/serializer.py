from rest_framework import serializers

from modules.api.v0.events.serializer import EventsSerializer
from modules.chat.models import Chat
from modules.users.serializer import UsersSerializer


class ChatSerializer(serializers.ModelSerializer):
    users = UsersSerializer(many=True, read_only=True)
    event = EventsSerializer(read_only=True, many=True)

    class Meta:
        model = Chat
        fields = ['users',
                  'event',
                  'photo_chat',
                  'personal_chat',
                  'date_created',
                  ]
