import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat, Message
from modules.events.models import Events
from modules.users.models import Users
from utils.answer import NO_DATA, MISSING_DATA, SUCCESS_SEND_MESSAGE

__all__ = ['SendMessageAPI', ]

LOGGER = logging.getLogger(__name__)


class SendMessageAPI(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Отправить сообщение в чат
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        event_id = request.data.get('event_id')
        message = request.data.get('message')

        if not all((event_id, message)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        event = Events.objects.get(id=event_id)
        try:
            chat = Chat.objects.get(event__id=event_id)
            Message.objects.create(sender=request.user,
                                   message=message,
                                   to_chat=chat)
        except:
            chat = Chat()
            chat.event = event
            ev = Events.objects.get(id=event_id)
            users = ev.list_users.all()
            creator_event = ev.create_event

            print('users ', users)
            print('creator_event ', creator_event)
            chat.save()

            if users:
                for user in users:
                    chat.users.add(Users.objects.get(id=user.id))

                for creator in creator_event:
                    chat.users.add(Users.objects.get(id=creator.id))

                chat.users.add(Users.objects.get(id=request.user.id))
            else:
                chat.users.add(Users.objects.get(id=creator_event.id))
                chat.users.add(Users.objects.get(id=request.user.id))

            Message.objects.create(sender=request.user,
                                   message=message,
                                   to_chat=chat)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_SEND_MESSAGE)
        )
