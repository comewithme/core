from django.urls import path

from modules.api.v0.chat.get_chats import MyChatsAPI
from modules.api.v0.chat.get_message_from_chat import GetMessageFromChatAPI

urlpatterns = [
    # get all chats
    path('', MyChatsAPI.as_view(), name='my_chats'),

    # create chat
    # path('create/', MyChatsAPI.as_view(), name='my_chats'),

    # create message in chat
    # path('send_message/', SendMessageAPI.as_view(), name='send_message'),
    # path('user/send_message/', SendMessageToUserAPI.as_view(), name='send_message_to_user'),

    # get message from chat
    path('messages/', GetMessageFromChatAPI.as_view(), name='get_message_from_chat'),

]
