import logging

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q

from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat, Message
from modules.events.models import Events
from modules.users.models import Users
from utils.answer import MISSING_DATA, SUCCESS_SEND_MESSAGE

__all__ = ['SendMessageToUserAPI', ]

LOGGER = logging.getLogger(__name__)


class SendMessageToUserAPI(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Отправить сообщение пользователю
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        user_id = request.data.get('user_id')
        message = request.data.get('message')

        if not all((user_id, message)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        from_user = Users.objects.get(id=request.user.id)
        to_user = Users.objects.get(id=user_id)

        try:
            chat = Chat.objects.get(Q(users=to_user) and Q(users=from_user) and Q(personal_chat=True))
            Message.objects.create(sender=request.user,
                                   message=message,
                                   to_chat=chat)
        except ObjectDoesNotExist:
            chat = Chat()
            chat.personal_chat = True
            chat.save()

            chat.users.add(from_user)
            chat.users.add(to_user)

            Message.objects.create(sender=request.user,
                                   message=message,
                                   to_chat=chat)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_SEND_MESSAGE)
        )
