import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.chat.models import Chat, Message
from utils.answer import NO_DATA

__all__ = ['MyChatsAPI', ]

LOGGER = logging.getLogger(__name__)


class MyChatsAPI(BaseAPIView):

    def get(self, request, *args, **kwargs):
        """
        Получить список чатов пользователя
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        chats = Chat.objects.filter(users__id=request.user.id)

        if chats is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(data=NO_DATA)
            )

        answer = []

        for ch in chats:
            chat = Chat.objects.get(id=ch.id)
            try:
                lat_message = Message.objects.filter(to_chat=chat)[0]
                latest_message = {
                    "id": lat_message.id,
                    'user_photo': lat_message.sender.get_photo(),
                    'user_name': lat_message.sender.__str__(),
                    'message': f'{lat_message.message}' if lat_message.message else '',
                    "date_created": int(lat_message.date_created.timestamp())
                }
            except:
                latest_message = None

            try:
                if chat.personal_chat:
                    to_user_chat = chat.users.exclude(id=request.user.id)
                    answer.append({
                        "chat_id": chat.id,
                        'chat_photo': chat.get_photo_chat(),
                        'chat_name': to_user_chat[0].first_name if to_user_chat[0].first_name else to_user_chat[0].phone,
                        'personal_chat': chat.personal_chat,
                        'date_created': int(chat.date_created.timestamp()),
                        'latest_message': latest_message
                    })
                else:
                    answer.append({
                        "chat_id": chat.id,
                        'chat_photo': chat.get_photo_chat(),
                        'chat_name': chat.__str__(),
                        'personal_chat': chat.personal_chat,
                        'date_created': int(chat.date_created.timestamp()),
                        'latest_message': latest_message
                    })
            except Exception as ex:
                LOGGER.error(f"[ERROR] {ex}")

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=answer)
        )
