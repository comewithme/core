import logging
from rest_framework import exceptions
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt import authentication

from modules.users.models import Users
from utils.answer import ACCESS_DENIED, ANSWER_LIST
from utils.utils import key_generator
from utils.answer import ERROR_DECODING_SIGNATURE, USER_NOT_FOUND
from utils.utils import decode_jwt_token

__all__ = ['BaseAPIView', 'CustomJWTAuthentication']

LOGGER = logging.getLogger('django')


class CustomJWTAuthentication(authentication.JSONWebTokenAuthentication):
    """
    Своя реализация сериализатора, которая авторизует используюя пользователя
    """

    def authenticate(self, request):
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            raise exceptions.AuthenticationFailed(ERROR_DECODING_SIGNATURE)
        payload = decode_jwt_token(jwt_value)
        if payload is None:
            raise exceptions.AuthenticationFailed(ERROR_DECODING_SIGNATURE)
        try:
            user = Users.objects.get(id=payload.get('user_id'))
        except Exception as ex:
            LOGGER.error(ex)
            raise exceptions.AuthenticationFailed(USER_NOT_FOUND)
        return user, jwt_value


class BaseAPIView(APIView):
    """
    Базовый класс APIView с нужными настройками
    """
    permission_classes = (IsAuthenticated,)
    authentication_classes = (CustomJWTAuthentication,)

    def __init__(self, *args, **kwargs):
        self.connection_id = key_generator(32)
        super().__init__(*args, **kwargs)

    def check_permissions(self, request):
        if self.request.user is None:
            raise exceptions.PermissionDenied(detail=ACCESS_DENIED)

    def get_answer(self, data=None, error=None):
        """
        Формируем ответ
        :param data:
        :param error:
        :param status_id:
        :return:
        """
        if error is not None:
            err_id = ANSWER_LIST.get(error)
            if err_id is None:
                err_id = 500
            answer = {"status": "error", "detail": None, "status_id": err_id, "error_message": error}
        else:
            err_id = 200
            answer = {"status": "ok", "detail": data, "status_id": err_id, "error_message": None}
        return answer


class ViewAPI(APIView):
    """
    Базовый класс APIView с нужными настройками
    """

    def __init__(self, *args, **kwargs):
        self.connection_id = key_generator(32)
        super().__init__(*args, **kwargs)

    def check_permissions(self, request):
        if self.request.user is None:
            raise exceptions.PermissionDenied(detail=ACCESS_DENIED)

    def get_answer(self, data=None, error=None):
        """
        Формируем ответ
        :param data:
        :param error:
        :param status_id:
        :return:
        """
        if error is not None:
            err_id = ANSWER_LIST.get(error)
            if err_id is None:
                err_id = 500
            answer = {"status": "error", "detail": None, "status_id": err_id, "error_message": error}
        else:
            err_id = 200
            answer = {"status": "ok", "detail": data, "status_id": err_id, "error_message": None}
        return answer
