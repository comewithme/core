import logging
from rest_framework.response import Response
from rest_framework import status
import os

from configs import MEDIA_ROOT
from modules.api.v0.base_api_view import BaseAPIView
from modules.users.models import Users
from datetime import datetime
from base64 import b64decode
from django.core.files.base import ContentFile
import uuid

__all__ = ['ChangeUserAPI', ]

from utils.answer import SUCCESS_CHANGE_USER, SERVICE_UNAVAILABLE, INCORRECT_BIRTHDAY, INCORRECT_EMAIL, INCORRECT_PHONE, \
    SUCCESS_REMOVE_PHOTO

LOGGER = logging.getLogger(__name__)


class ChangeUserAPI(BaseAPIView):

    def patch(self, request, *args, **kwargs):
        """
        Обновление/изменение данных у пользователя в профиле
        """

        name = request.data.get('name')
        phone = request.data.get('phone')
        birthday = request.data.get('birthday')
        gender = request.data.get('gender')
        photo = request.data.get('photo')
        about_me = request.data.get('about_me')
        instagram = request.data.get('instagram')
        vk = request.data.get('vk')
        telegram = request.data.get('telegram')

        try:
            if birthday:
                birthday = int(birthday)
                birthday = datetime.fromtimestamp(int(birthday)).strftime("%Y-%m-%d")
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_answer(data=f"{INCORRECT_BIRTHDAY} со значением {birthday}"))

        try:
            user = Users.objects.get(id=request.user.id)

            if name is not None:
                user.first_name = name
            if telegram is not None and telegram != "" and telegram != " ":
                user.telegram = telegram
            if instagram is not None and instagram != "" and instagram != " ":
                user.instagram = instagram
            if vk is not None and vk != "" and vk != " ":
                user.vk = vk
            if phone is not None:
                if Users.objects.filter(phone=phone) and user.phone != phone and phone != "" and phone != " ":
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=self.get_answer(error=INCORRECT_PHONE))
                else:
                    user.phone = phone
            if birthday is not None:
                user.birthday = birthday
            if gender is not None:
                user.gender = gender
            if photo is not None:
                image_data = b64decode(photo)
                image_name = str(uuid.uuid4()) + ".jpg"
                user.photo = ContentFile(image_data, image_name)
            if about_me is not None:
                user.about_me = about_me
            user.save()
        except Exception as ex:
            LOGGER.error(ex)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=SERVICE_UNAVAILABLE))

        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SUCCESS_CHANGE_USER))

    def delete(self, request, *args, **kwargs):
        user = Users.objects.get(id=request.user.id)
        try:
            if user.photo:
                path_to_photo = MEDIA_ROOT + '/' + str(user.photo)
                os.remove(path_to_photo)
        except Exception as ex:
            LOGGER.error(f"photo user {user} not exist: {ex}")
        user.photo = "avatar.png"
        user.save()
        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SUCCESS_REMOVE_PHOTO))
