import logging

from fcm_django.models import FCMDevice
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView

__all__ = ['ConfigFCM', ]

from utils.answer import MISSING_DATA, SUCCESS_ADD_FCM_TOKEN, FCM_TOKEN_ALREADY_EXITS

LOGGER = logging.getLogger(__name__)


class ConfigFCM(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Добавление fcm токена
        :param request:
        :return Response:
        """

        registration_id = request.data.get('registration_id')
        type_device = request.data.get('type')
        device_id = request.data.get('device_id')

        # FCMDevice
        if not all((registration_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        if FCMDevice.objects.filter(registration_id=registration_id):
            fcm_token = FCMDevice.objects.get(registration_id=registration_id)
            if request.user != fcm_token.user:
                fcm_token.delete()
                FCMDevice.objects.create(registration_id=registration_id,
                                         type=type_device,
                                         device_id=device_id,
                                         user=request.user)
            else:
                return Response(
                    status=status.HTTP_200_OK,
                    data=self.get_answer(data=FCM_TOKEN_ALREADY_EXITS)
                )
        else:
            try:
                FCMDevice.objects.create(registration_id=registration_id,
                                         type=type_device,
                                         device_id=device_id,
                                         user=request.user)
            except Exception as ex:
                LOGGER.error(f"Can not create fcm token: {ex}")

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_ADD_FCM_TOKEN)
        )
