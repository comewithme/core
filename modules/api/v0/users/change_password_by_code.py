import logging

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.users.models import Users
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from configs import settings

__all__ = ['ChangePasswordByCode', ]

from utils.answer import MISSING_DATA, CODE_NOT_VALID, SERVICE_UNAVAILABLE, SUCCESS_CHANGE_USER_PASSWORD
from utils.utils import send_email

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
LOGGER = logging.getLogger(__name__)


class ChangePasswordByCode(BaseAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        """
        Смена пароля по коду
        """

        code = request.data.get('code')
        new_password = request.data.get('new_password')

        if not all((code, new_password,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        for key in cache.keys('*'):
            if isinstance(cache.get(key), dict):
                if cache.get(key)['code'] == code:
                    try:
                        user = Users.objects.get(phone=key)
                        user.set_password(new_password)
                        user.save()
                        LOGGER.info(f"Success change password {key}")
                        return Response(status=status.HTTP_200_OK,
                                        data=self.get_answer(data=SUCCESS_CHANGE_USER_PASSWORD))
                    except Exception as ex:
                        LOGGER.error(f'I can not change password: {ex}')
                        return Response(status=status.HTTP_400_BAD_REQUEST,
                                        data=self.get_answer(error=SERVICE_UNAVAILABLE))
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=CODE_NOT_VALID))

        return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=CODE_NOT_VALID))
