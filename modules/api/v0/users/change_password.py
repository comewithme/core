import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.users.models import Users

__all__ = ['ChangePassword', ]

from utils.answer import SERVICE_UNAVAILABLE, MISSING_DATA, INCORRECT_NEW_PASSWORD, SUCCESS_CHANGE_USER_PASSWORD

LOGGER = logging.getLogger(__name__)


class ChangePassword(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """
        Изменение пароля у пользователя
        """

        password = request.data.get('password')

        if not all((password,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )
        if len(password) < 6:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=INCORRECT_NEW_PASSWORD)
            )
        user = Users.objects.get(id=request.user.id)

        try:
            user.set_password(password)
            user.save()
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=SERVICE_UNAVAILABLE))

        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SUCCESS_CHANGE_USER_PASSWORD))
