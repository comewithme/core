import logging

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
import random

from modules.api.v0.base_api_view import BaseAPIView
from modules.users.models import Users
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from configs import settings

__all__ = ['SendCodeForChangePassword', ]

from utils.answer import MISSING_DATA, USER_NOT_FOUND, SUCCESS_CODE, CODE_NOT_VALID, MANY_SEND_SMS, SEND_CODE
from utils.utils import send_sms

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
LOGGER = logging.getLogger(__name__)


class SendCodeForChangePassword(BaseAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        """
        Отправка кода на номер для подтверждения номера и смены пароля
        """

        phone = request.data.get('phone')
        random_number = random.randint(1000, 9999)
        message = f"Код подтверждения: {random_number}"
        if not all((phone,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            user = Users.objects.get(phone=phone)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=USER_NOT_FOUND)
            )

        # save in cache
        if cache.get(phone):
            if cache.get(phone)['count'] < 3:
                new_count = cache.get(phone)['count'] + 1
                value = {"code": random_number, "count": new_count}
                cache.set(phone, value, 300)
                LOGGER.info(f'Cached for user {user}: {cache.get(phone)}')
                # send_sms(phone, message)
                return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SEND_CODE))
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=MANY_SEND_SMS))
        else:
            value = {"code": random_number, "count": 1}
            cache.set(phone, value, 300)
            LOGGER.info(f'Cached for {phone}: {cache.get(phone)}')
            send_sms(phone, message)
        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SEND_CODE))


class CheckCode(BaseAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        """
        Проверка кода для смены пароля
        """

        code = request.data.get('code')

        if not all((code,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        try:
            code = int(code)
            LOGGER.info(f"code: {code} and type {type(code)}")
        except:
            LOGGER.error(f"Can't convert in int: {code}")

        try:
            for key in cache.keys('*'):
                if isinstance(cache.get(key), dict):
                    if cache.get(key)['code'] == code:
                        return Response(status=status.HTTP_200_OK, data=self.get_answer(data=SUCCESS_CODE))
        except Exception as ex:
            LOGGER.error(f'Error: {ex}')

        return Response(status=status.HTTP_400_BAD_REQUEST, data=self.get_answer(error=CODE_NOT_VALID))
