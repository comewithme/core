from rest_framework import serializers

from configs import settings
from modules.api.v0.events.serializer import EventsSerializer
from modules.ratings.models import Reviews
from modules.users.serializer import UsersSerializer


class ReviewsSerializer(serializers.ModelSerializer):
    user_from = UsersSerializer()
    user_to = UsersSerializer()
    event = EventsSerializer()

    class Meta:
        model = Reviews
        fields = ['id',
                  'user_from',
                  'user_to',
                  'event',
                  'star_for_event',
                  'description',
                  ]