from django.urls import path

from modules.api.v0.users.change_password_by_code import ChangePasswordByCode
from modules.api.v0.users.change_user import ChangeUserAPI
from modules.api.v0.users.change_password import ChangePassword
from modules.api.v0.users.fcm_device import ConfigFCM
from modules.api.v0.users.send_code_for_change_password import SendCodeForChangePassword, CheckCode
from modules.api.v0.users.views import UserViewApi

urlpatterns = [
    path('', UserViewApi.as_view(), name='api_user'),
    path('change/', ChangeUserAPI.as_view(), name='change_user'),
    path('fcm-device/', ConfigFCM.as_view(), name='fcm-device'),

    # change password into profile
    path('password/change/', ChangePassword.as_view(), name='change_password'),

    # change password by code from sms
    path('password/send_code/', SendCodeForChangePassword.as_view(), name='send_code_change_password'),
    path('password/check_code/', CheckCode.as_view(), name='check_code'),
    path('password/change_password/', ChangePasswordByCode.as_view(), name='change_password_by_user'),
]
