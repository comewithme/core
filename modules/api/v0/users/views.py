import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.ratings.models import UserRatings
from modules.users.models import Users
from django.conf import settings

import time
import datetime

__all__ = ['UserViewApi', ]

from utils.answer import SUCCESS_CHANGE_USER, SERVICE_UNAVAILABLE

LOGGER = logging.getLogger(__name__)


class UserViewApi(BaseAPIView):

    def get(self, request, *args, **kwargs):
        try:
            photo = '{0}{1}'.format(settings.BASE_HOST, self.request.user.photo.url)
        except:
            photo = None
        try:
            total_rating = UserRatings.objects.get(user=self.request.user.id)
        except:
            total_rating = UserRatings()
            total_rating.user = request.user
            total_rating.total_popularity = 0.0
            total_rating.number_events_visited = 0
            total_rating.save()

        content = {
            "name": request.user.first_name,
            "phone": request.user.phone,
            "email": request.user.email,
            "telegram": request.user.telegram if request.user.telegram else '',
            "vk": request.user.vk if request.user.vk else '',
            "instagram": request.user.instagram if request.user.instagram else '',
            "about_me": request.user.about_me if request.user.about_me else '',
            "gender": request.user.gender,
            "birthday": int(time.mktime(datetime.datetime.strptime(f'{request.user.birthday}',
                                                                   "%Y-%m-%d").timetuple())) if request.user.birthday else 0,
            "photo": photo,
            "total_rating": total_rating.total_popularity
        }
        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(content)
        )