from django.urls import path

from modules.api.v0.complaint.complaint_on_event import ComplaintEventApi
from modules.api.v0.complaint.complaint_on_user import ComplaintUserApi

urlpatterns = [
    path('user/', ComplaintUserApi.as_view(), name='complaint_user'),
    path('event/', ComplaintEventApi.as_view(), name='complaint_event'),
]
