import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.core.models.complaint import ComplaintUser

__all__ = ['ComplaintUserApi', ]

from modules.users.models import Users

from utils.answer import MISSING_DATA, USER_NOT_FOUND, SUCCESS_CREATE_COMPLAINT, ALREADY_SEND_COMPLAINT

LOGGER = logging.getLogger(__name__)


class ComplaintUserApi(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """Создание жалобы на пользователя."""

        user_id = request.data.get('user_id')

        if not all((user_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        from_user = Users.objects.get(id=request.user.id)

        try:
            to_user = Users.objects.get(id=user_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=USER_NOT_FOUND)
            )

        try:
            complaint_on_user = ComplaintUser.objects.get(to_user=to_user)

            if request.user not in complaint_on_user.from_user.all():
                complaint_on_user.from_user.add(from_user)
                complaint_on_user.save()
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=self.get_answer(error=ALREADY_SEND_COMPLAINT)
                )
        except:
            complaint_on_user = ComplaintUser()
            complaint_on_user.to_user = to_user
            complaint_on_user.save()
            complaint_on_user.from_user.add(from_user)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_CREATE_COMPLAINT)
        )
