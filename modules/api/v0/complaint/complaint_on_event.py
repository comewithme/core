import logging
from rest_framework.response import Response
from rest_framework import status

from modules.api.v0.base_api_view import BaseAPIView
from modules.core.models.complaint import ComplaintEvent

__all__ = ['ComplaintEventApi', ]

from modules.events.models import Events

from modules.users.models import Users

from utils.answer import MISSING_DATA, SUCCESS_CREATE_COMPLAINT, EVENT_NOT_FOUND, ALREADY_SEND_COMPLAINT

LOGGER = logging.getLogger(__name__)


class ComplaintEventApi(BaseAPIView):

    def post(self, request, *args, **kwargs):
        """Создание жалобы на событие."""

        event_id = request.data.get('event_id')

        if not all((event_id,)):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=MISSING_DATA)
            )

        from_user = Users.objects.get(id=request.user.id)

        try:
            event = Events.objects.get(id=event_id)
        except:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_answer(error=EVENT_NOT_FOUND)
            )

        try:
            complaint_on_event = ComplaintEvent.objects.get(to_event=event)

            if request.user not in complaint_on_event.from_user.all():
                complaint_on_event.from_user.add(from_user)
                complaint_on_event.save()

                info_event = ComplaintEvent.objects.filter(to_event=event).values_list('from_user')

                if len(info_event) >= 5:
                    event = Events.objects.get(id=complaint_on_event.to_event.id)
                    event.is_active = False
                    event.save()
                    LOGGER.info('Event {0} becomes inactive'.format(complaint_on_event.to_event))
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=self.get_answer(error=ALREADY_SEND_COMPLAINT)
                )

        except:
            complaint_on_event = ComplaintEvent()
            complaint_on_event.to_event = event
            complaint_on_event.save()
            complaint_on_event.from_user.add(from_user)

        return Response(
            status=status.HTTP_200_OK,
            data=self.get_answer(data=SUCCESS_CREATE_COMPLAINT)
        )
