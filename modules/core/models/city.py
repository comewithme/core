from django.db import models


class City(models.Model):
    name = models.CharField(verbose_name='Название города', max_length=100, unique=True)
    latitude = models.FloatField(verbose_name='Координата по x', help_text='Пример 54.7410')
    longitude = models.FloatField(verbose_name='Координата по y', help_text='Пример 55.9889')

    class Meta:
        verbose_name = "Список городов"
        verbose_name_plural = 'Список городов'

    def __str__(self):
        return self.name
