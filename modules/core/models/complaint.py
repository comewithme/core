from django.conf import settings
from django.db import models

from modules.events.models import Events


class ComplaintUser(models.Model):
    from_user = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name='От кого')
    to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='complaint_user', verbose_name='Жалоба на кого',
                                on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Жалоба пользователя'
        verbose_name_plural = 'Жалоба пользователям'

    def __str__(self):
        return f'Жалоба на пользователя: {self.to_user}'


class ComplaintEvent(models.Model):
    from_user = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name='От кого')
    to_event = models.ForeignKey(Events, related_name='complain_event', verbose_name='Жалоба на событие',
                                 on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Жалоба на событие'
        verbose_name_plural = 'Жалоба на события'

    def __str__(self):
        return f'Жалоба на событие: {self.to_event}'