from django.core.management.base import BaseCommand

from modules.events.models import Events
import logging

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Remove all events'

    def handle(self, *args, **options):
        try:
            events = Events.objects.all()
            for ev in events:
                ev.delete()
        except Exception as ex:
            LOGGER.error(f"Can not remove {ev}: {ex} ")