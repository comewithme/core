from django.core.management.base import BaseCommand
from fcm_django.models import FCMDevice

import logging

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send push'

    def handle(self, *args, **options):
        SECRET_KEY = "AAAAh71v008:APA91bHJdAthaVILK4JiE6goXxS_9XuEcXwwyBkn9PXpPuYbU8TjFdXfgQfQMMArj5iCIXP33-dK5jPukFWYDgumGDbIiNqhx4QraCfYCgV70MzyCDC_kTYRMlmudW9DflBH07xEoR7n"
        devices = FCMDevice.objects.all()

        for dev in devices:
            try:
                dev.send_message(title='test', body='body test', api_key=SECRET_KEY)
            except:
                pass
