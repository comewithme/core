from django.core.management.base import BaseCommand

from utils.category import create_or_update_category
from utils.color import create_or_update_color
from utils.city import create_or_update_city
import logging

from utils.content import get_or_create_user, update_content_from_kudago
from utils.timepad import update_content_from_timepad

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update server'

    def handle(self, *args, **options):

        try:
            create_or_update_city()
        except Exception as ex:
            LOGGER.error(f"[ERROR] Can't create or update city: Error:{ex}")

        try:
            create_or_update_color()
        except Exception as ex:
            LOGGER.error(f"[ERROR] Can't create or update color: Error:{ex}")

        try:
            create_or_update_category()
        except Exception as ex:
            LOGGER.error(f"[ERROR] Can't create or update category: Error:{ex}")

        try:
            update_content_from_kudago()
        except Exception as ex:
            LOGGER.error(f"[ERROR] Can't create or update events: Error:{ex}")

        try:
            get_or_create_user(created=True)
        except Exception as ex:
            LOGGER.error(f"[ERROR] Can't create or update events: Error:{ex}")

        try:
            update_content_from_timepad()
        except Exception as ex:
            LOGGER.error(f"[ERROR] Can't create or update events: Error:{ex}")
