from django.core.management.base import BaseCommand

from modules.events.models import Tags, ColorGradient
import logging

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Delete color'

    def handle(self, *args, **options):
        for i in ColorGradient.objects.all():
            i.delete()
