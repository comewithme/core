from django.contrib import admin

from modules.core.models.city import City
from modules.core.models.complaint import ComplaintUser, ComplaintEvent


@admin.register(ComplaintUser)
class ComplaintUserAdmin(admin.ModelAdmin):
    pass


@admin.register(ComplaintEvent)
class ComplaintEventAdmin(admin.ModelAdmin):
    pass


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ["name", "latitude", "longitude"]
    search_fields = ['name', ]
