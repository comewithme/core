from django.contrib import admin

from modules.ratings.models import UserRatings, Reviews


@admin.register(UserRatings)
class UserRatingsAdmin(admin.ModelAdmin):
    list_display = ['user', 'total_popularity', 'number_events_visited']
    search_fields = ['user', ]


@admin.register(Reviews)
class ReviewsAdmin(admin.ModelAdmin):
    list_display = ['user_from', 'user_to', 'event', 'star_for_event', 'description', ]
    search_fields = ['user', ]

