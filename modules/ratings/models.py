from django.conf import settings
from django.db import models
from django.utils import timezone

from modules.events.models import Events


class Reviews(models.Model):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5

    STAR = (
        (ONE, 1),
        (TWO, 2),
        (THREE, 3),
        (FOUR, 4),
        (FIVE, 5),
    )

    user_from = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_from', verbose_name='Кто оставил',
                                  on_delete=models.CASCADE)
    user_to = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_to', verbose_name='Кому',
                                on_delete=models.CASCADE)
    event = models.ForeignKey(Events, on_delete=models.CASCADE, verbose_name='Событие')
    star_for_event = models.PositiveSmallIntegerField('Оценка', choices=STAR, db_index=True)
    description = models.TextField(verbose_name='Пояснение', blank=True, null=True)
    date_create = models.DateTimeField(verbose_name='Дата создания события', default=timezone.now, null=True)

    class Meta:
        verbose_name = 'Рейтинг пользователя за событие'
        verbose_name_plural = 'Рейтинги пользователей за события'


class UserRatings(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Пользователь', on_delete=models.CASCADE)
    total_popularity = models.FloatField(verbose_name='Общая оценка пользователя')
    number_events_visited = models.IntegerField(verbose_name='Количество посещенных событий')
    list_reviews = models.ManyToManyField(Reviews, verbose_name='Список отзывов', blank=True)

    class Meta:
        verbose_name = 'Рейтинг пользователя'
        verbose_name_plural = 'Рейтинг пользователей'
