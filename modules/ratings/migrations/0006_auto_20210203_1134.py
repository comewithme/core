# Generated by Django 3.1.5 on 2021-02-03 08:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ratings', '0005_auto_20210131_1329'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviews',
            name='user',
        ),
        migrations.AddField(
            model_name='reviews',
            name='user_from',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='user_from', to='users.users', verbose_name='Кто оставил'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reviews',
            name='user_to',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='user_to', to='users.users', verbose_name='Кому'),
            preserve_default=False,
        ),
    ]
