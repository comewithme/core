# Generated by Django 3.1.5 on 2021-01-25 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ratings', '0003_auto_20210125_1958'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userratings',
            name='total_popularity',
            field=models.IntegerField(verbose_name='Общая оценка пользователя'),
        ),
    ]
