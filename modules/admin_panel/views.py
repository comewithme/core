from django.views.generic import TemplateView, FormView
import logging

from modules.access_the_events.models import AccessForEvents
from modules.admin_panel.forms import EventsForm
from modules.events.models import ColorGradient

LOGGER = logging.getLogger(__name__)


class CreateEvent(TemplateView, FormView):
    form_class = EventsForm
    template_name = 'admin_panel/event.html'
    success_url = '/admin_panel/event/create/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["colors"] = ColorGradient.objects.all()
        return context

    def form_valid(self, form):
        event = form.save(commit=False)
        event.create_event = self.request.user
        event.save()

        if not AccessForEvents.objects.filter(user=self.request.user,
                                              event=event,
                                              access=AccessForEvents.CREATED):
            AccessForEvents.objects.create(user=self.request.user,
                                           event=event,
                                           access=AccessForEvents.CREATED)
        return super(CreateEvent, self).form_valid(form)
