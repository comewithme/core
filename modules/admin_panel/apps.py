from django.apps import AppConfig


class AdminPanelConfig(AppConfig):
    name = 'modules.admin_panel'
