from django import forms

from modules.access_the_events.models import AccessForEvents
from modules.events.models import Events
import logging

LOGGER = logging.getLogger(__name__)


class EventsForm(forms.ModelForm):
    class Meta:
        model = Events
        fields = [
            "name",
            "description",
            "address",
            "color",
            "coordinate_x",
            "coordinate_y",
            "accept_automatic",
            "url_site",
            "category",
            "price",
            "max_count_users",
            "photo_event",
            "is_online",
            "date_event",
        ]

    def clean(self):
        cleaned_data = super(EventsForm, self).clean()
        name = cleaned_data.get("name")
        description = cleaned_data.get("description")
        address = cleaned_data.get("address")
        color = cleaned_data.get("color")
        coordinate_x = cleaned_data.get("coordinate_x")
        coordinate_y = cleaned_data.get("coordinate_y")
        accept_automatic = cleaned_data.get("accept_automatic")
        url_site = cleaned_data.get("url_site")
        category = cleaned_data.get("category")
        price = cleaned_data.get("price")
        photo_event = cleaned_data.get("photo_event")
        is_online = cleaned_data.get("is_online")
        date_event = cleaned_data.get("date_event")

        try:
            already_exist_event = Events.objects.filter(name=name,
                                                        description=description,
                                                        address=address,
                                                        category=category,
                                                        price=price,
                                                        date_event=date_event)
            if already_exist_event:
                raise forms.ValidationError("Событие уже существует в базе данных")
        except Exception as ex:
            LOGGER.error(f'Can not create event: {ex}')
        return cleaned_data
