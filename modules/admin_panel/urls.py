from django.urls import path

from .views import CreateEvent

urlpatterns = [
    path('event/create/', CreateEvent.as_view(), name='create_event_in_admin_panel'),
]
