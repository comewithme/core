from modules.events.models import ColorGradient
import logging

LOGGER = logging.getLogger(__name__)


def create_or_update_color():
    start_color = [
        '#6F0B6B',
        '#726A83',
        '#01579B',
        '#FF6F00',
        '#FFF3E0',
        '#B71C1C',
        '#2F0743',
        '#7F5AF0',
        '#AF2116',
        '#512DA8',
        '#388E3C',
        '#26A69A',
        '#C2185B',
        '#3C1053',
    ]

    end_color = [
        '#EF6C1E',
        '#E9E4F0',
        '#FF6F00',
        '#FFF3E0',
        '#B71C1C',
        '#573679',
        '#7F5AF0',
        '#0E71BB',
        '#512DA8',
        '#388E3C',
        '#26A69A',
        '#C2185B',
        '#AD5389',
    ]

    color_text = [
        '#F8F8F8',
        '#00214D',
        '#F8F8F8',
        '#00214D',
        '#00214D',
        '#F8F8F8',
        '#F8F8F8',
        '#F8F8F8',
        '#F8F8F8',
        '#F8F8F8',
        '#F8F8F8',
        '#00214D',
        '#F8F8F8',
        '#F8F8F8',
    ]

    color_icon = [
        '#FDE24F',
        '#FDE24F',
        '#FDE24F',
        '#00214D',
        '#00214D',
        '#FDE24F',
        '#FDE24F',
        '#FDE24F',
        '#FDE24F',
        '#FDE24F',
        '#FDE24F',
        '#00214D',
        '#FDE24F',
        '#FDE24F',
    ]

    for color_s, color_e, color_t, color_i in zip(start_color, end_color, color_text, color_icon):
        c_g, created = ColorGradient.objects.get_or_create(
            start_color=color_s,
            end_color=color_e,
            color_text=color_t,
            color_icon=color_i,
        )
        if created:
            LOGGER.info(f'Color created {c_g}')
