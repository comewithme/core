from urllib.parse import urljoin

import requests
import logging

from configs import settings
from modules.core.models.city import City
from modules.events.models import Category, Events
from utils.citys_coordinate import check_city_and_get_coordinate
from utils.content import get_or_create_user, random_name_event
from utils.utils import create_event_or_online, save_img_from_url

LOGGER = logging.getLogger(__name__)


def update_content_from_timepad():
    LOGGER.info('Start add content from TimePad API')
    params = f"?fields=ends_at,location,description_short,status,ticket_types&starts_at_min=now&category_ids=217,374,376,382,452,453,457,460,461,462,463,524,1315,1940,2335&starts_at_max=now+3day"
    url = urljoin(settings.TIMEPAD_API, params)

    headers = {"Authorization": settings.TIMEPAD_TOKEN}
    try:
        req = requests.get(url, headers=headers).json()
    except Exception as ex:
        LOGGER.debug(f"[ERROR GET EVENT] Error get events from TimePad API: {ex}")

    if req['total'] != 0:
        for value in req["values"]:
            if value["status"] != "ok":
                pass
            else:
                random_user_for_event = get_or_create_user(random_user=True)
                print(value['name'])
                if not Events.objects.filter(description=value["description_short"], url_site=value["url"]):
                    if value["location"]["city"] == "Без города":
                        name = f"{random_name_event()} {value['name']}"
                        description = value["description_short"]
                        url_site = value["url"]
                        try:
                            category = Category.objects.get(name="Отдых")
                        except:
                            category = ''
                        try:
                            price = int(value["ticket_types"]["price"])
                        except:
                            price = 0
                        date_event = value["starts_at"]

                        url = value["poster_image"]["default_url"]
                        photo_event = save_img_from_url(url)
                        create_event_or_online(name=name, description=description, url_site=url_site, category=category,
                                               price=price, user_create_event=random_user_for_event,
                                               photo_event=photo_event,
                                               date_event=date_event, event_online=True)
                    else:
                        name = f"{random_name_event()} {value['name']}"
                        description = value["description_short"]
                        url_site = value["url"]
                        try:
                            category = Category.objects.get(name="Отдых")
                        except:
                            category = ''
                        try:
                            price = value["ticket_types"][0]["price"]
                        except:
                            price = 0
                        date_event = value["starts_at"]
                        address = value["location"]["city"]
                        url = value["poster_image"]["default_url"]
                        photo_event = save_img_from_url(url)
                        if City.objects.filter(name=value['location']['city']):
                            city = City.objects.get(name=value['location']['city'])
                            city_latitude = city.latitude
                            city_longitude = city.longitude
                        else:
                            data_city = check_city_and_get_coordinate(value["location"]['city'])
                            city_latitude = data_city.latitude
                            city_longitude = data_city.longitude

                        create_event_or_online(name=name, description=description, url_site=url_site, category=category,
                                               price=price, user_create_event=random_user_for_event, address=address,
                                               coordinate_x=city_latitude, coordinate_y=city_longitude,
                                               photo_event=photo_event, date_event=date_event, event_online=False)
