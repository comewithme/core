from modules.events.models import Category
import logging

LOGGER = logging.getLogger(__name__)


def create_or_update_category():
    category = [
        "Спорт",
        "Развлечения",
        "Отдых",
        "Природа",
        "Прогулка",
        "Образование",
        "Знакомства",
        "Здоровье",
        "Криптовалюта",
        "Бизнес",
        "Онлайн",
        "Для детей",
    ]

    for cat in category:
        c_o, created = Category.objects.get_or_create(name=cat)

        if created:
            LOGGER.info(f"[CATEGORY] Category created: {created}")
