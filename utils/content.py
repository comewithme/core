import requests
from django.utils import timezone

import re

from configs import settings
from modules.access_the_events.models import AccessForEvents
from modules.events.models import Events
import logging
from datetime import datetime
import random
from modules.users.models import Users
from utils.utils import get_img_from_url

LOGGER = logging.getLogger(__name__)

now = datetime.today()
now_timestamp = now.timestamp()
after_three_days = now + timezone.timedelta(3)
after_three_days_timestamp = after_three_days.timestamp()
name_event = ''
description = ''
address = ''
coordinate_x = ''
coordinate_y = ''
accept_automatic = ''
url_site = ''
price = ''

try:
    cwm_user = Users.objects.get(phone=9174503591)
except:
    Users.objects.get_or_create(phone=9174503591,
                                first_name='Evento',
                                password='aQEdEf4408')
date_event = []
photo_event = ''


def random_name_event():
    name_for_event = [
        "Сходить на ",
        "Го со мной ",
        "Поучаствовать в ",
        "Кто хочет сходить на ",
        "Кто присоединиться на ",
        "Ищу с кем пойти ",
        "Кто со мной на",
        "Составьте компанию на ",
    ]
    return random.choice(name_for_event)


def get_or_create_user(random_user=None, created=None):
    phone_number = ['9174503550', '9174503551', '9174503552', '9174503553', '9174503554', '9174503555', '9174503556',
                    '9174503557', '9174503558', '9174503559']
    if created:
        first_name = ['Мария', 'Андрей', 'Дарья', 'Паша', 'Настя', 'Няша', 'Настя', 'Кристина', 'Рита']
        birthday = ["1990-04-5", "1993-06-12", "1994-08-20", "1994-08-28", "1994-10-20", "1994-4-20", "1994-1-2",
                    "1994-08-29", "1994-12-30"]
        for name, phone, birth in zip(first_name, phone_number, birthday):
            try:
                user = Users.objects.create_user(first_name=name,
                                                 birthday=birth,
                                                 phone=phone)
                user.set_password("Otubeh9221")
                user.save()
            except Exception as ex:
                LOGGER.error(f"[ERROR] Can't create user: {ex}")
    if random_user:
        random_phone = random.choice(phone_number)
        try:
            Users.objects.get(phone=random_phone)
        except:
            user = Users.objects.create_user(phone=random_phone)
            user.set_password("Otubeh9221")
            user.save()
        return Users.objects.get(phone=random_phone)


def update_content_from_kudago():
    get_or_create_user(created=True)
    LOGGER.info('Start add content')
    params = f"fields=dates,title,place,description,body_text,price,images,site_url,location&text_format=text&actual_since={now_timestamp}&actual_until={after_three_days_timestamp}&expand=participants,place,location&categories=cinema,education,festival,holiday,party,quest,recreation"
    try:
        req = requests.get(f"{settings.KUDAGO_API}{params}").json()
    except Exception as ex:
        LOGGER.debug(f"[ERROR GET EVENT] Error get events from kudago: {ex}")
    try:
        if req['results'] and req['count'] != 0:
            for event in req['results']:
                name_event = event['title']
                description = event['description']
                address = event['place']['address'] if not event['place'] is None else event['location']['name']
                coordinate_x = event['location']['coords']['lat']
                coordinate_y = event['location']['coords']['lon']

                try:
                    if event["price"] != "" and not event["price"] is None:
                        price = int(re.search(r'\d+', event["price"]).group())
                    else:
                        price = 0
                except:
                    price = 0

                count_event_dates = 0
                for i in event["dates"]:
                    if i['start'] > now_timestamp and count_event_dates < 1:
                        count_event_dates += 1
                        if not Events.objects.filter(name=name_event,
                                                     date_event=datetime.fromtimestamp(i['start'])):
                            if event["images"]:
                                photo_event_url = event["images"][0]['image']
                                url = f'{photo_event_url}'
                                photo_event = get_img_from_url(url)
                        else:
                            if Events.objects.filter(name=name_event):
                                photo_event = Events.objects.filter(name=name_event)[0].photo_event
                        random_user_for_event = get_or_create_user(random_user=True)
                        if event["location"]["slug"] == "online":
                            if not Events.objects.filter(description=description,
                                                         address=address,
                                                         url_site=url_site,
                                                         price=price,
                                                         date_event=datetime.fromtimestamp(i['start'])
                                                         ):
                                ev = Events.objects.get_or_create(
                                    name=f"{random_name_event()} {name_event}",
                                    description=description,
                                    address=address,
                                    url_site=url_site,
                                    price=price,
                                    create_event=random_user_for_event,
                                    photo_event=photo_event,
                                    is_online=True,
                                    date_event=datetime.fromtimestamp(i['start'])
                                )
                                access_event = Events.objects.get(id=ev[0].id)
                                AccessForEvents.objects.get_or_create(
                                    user=cwm_user,
                                    event=access_event,
                                    access=AccessForEvents.CREATED
                                )
                                LOGGER.info(f'Create event {ev}')
                        else:
                            if not Events.objects.filter(description=description,
                                                         address=address,
                                                         url_site=url_site,
                                                         price=price,
                                                         date_event=datetime.fromtimestamp(i['start'])
                                                         ):
                                ev = Events.objects.get_or_create(
                                    name=f"{random_name_event()} {name_event}",
                                    description=description,
                                    address=address,
                                    coordinate_x=coordinate_x,
                                    coordinate_y=coordinate_y,
                                    url_site=url_site,
                                    price=price,
                                    create_event=random_user_for_event,
                                    photo_event=photo_event,
                                    date_event=datetime.fromtimestamp(i['start'])
                                )
                                access_event = Events.objects.get(id=ev[0].id)
                                AccessForEvents.objects.get_or_create(
                                    user=random_user_for_event,
                                    event=access_event,
                                    access=AccessForEvents.CREATED
                                )
                                LOGGER.info(f'Create event {ev}')
    except Exception as ex:
        LOGGER.error(f"[ERROR GET EVENT] [DEBUG] No events: {ex}")
