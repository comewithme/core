import random
import jwt
from django.conf import settings
from django.utils import timezone as tz
from string import ascii_letters
from django.core.mail import send_mail
import requests
from configs import FCM_SERVER_KEY, SMS_URL, SMS_USER, SMS_PASSWORD
from modules.access_the_events.models import AccessForEvents
from modules.chat.models import Chat
from modules.events.models import Events
from utils.regulars import del_html_tags

import urllib.request
import base64
from base64 import b64decode
import uuid
from django.core.files.base import ContentFile

__all__ = ['del_html', 'key_generator', 'decode_jwt_token', 'get_img_from_url']

import logging

LOGGER = logging.getLogger(__name__)


def del_html(text):
    """
    Удаление html тегов из текста
    :param text:
    :return:
    """
    if text is None:
        return
    return del_html_tags.sub('', text)


def key_generator(length):
    """Генератор случайной строки"""
    return ''.join(random.choice(ascii_letters) for i in range(length))


def decode_jwt_token(token):
    """
    Проверка ключа
    :param token: string
    :return: dict or None
    """
    time = tz.now().timestamp()
    try:
        decode = jwt.decode(
            token,
            settings.JWT_SECRET_KEY
        )
    except:
        return

    # Проверка на протухание
    exp = decode.get('exp')
    if exp is None or exp < time:
        return None
    return decode


def get_img_from_url(url):
    """Получение изображения из ссылки"""
    contents = urllib.request.urlopen(url).read()
    data_photo = base64.b64encode(contents)
    image_data = b64decode(data_photo)
    image_name = str(uuid.uuid4()) + ".jpg"
    return ContentFile(image_data, image_name)


def save_img_from_url(url):
    import urllib.request
    filename = f"{uuid.uuid4()}.jpg"
    filepath = f"{settings.MEDIA_ROOT}/{filename}"
    img = urllib.request.urlretrieve(url, filepath)
    return filename


def send_push_fcm_device(devices, text):
    devices.send_message(title="Найдено новое событие!",
                         body=f"{text}",
                         api_key=FCM_SERVER_KEY)


def send_email(title, body_text, email_user):
    try:
        send_mail(
            f'{title}',
            f'{body_text}',
            'info@comewithme.ru',
            [f'{email_user}'],
            fail_silently=True,
        )
    except Exception as ex:
        LOGGER.debug(f'Can not send email: {ex}')


def send_sms(phone, message):
    try:
        r = requests.get(f"{SMS_URL}?login={SMS_USER}&psw={SMS_PASSWORD}&phones=7{phone}&mes={message}")
    except Exception as ex:
        LOGGER.error(f"[ERROR SMS] {ex}")


def create_event_or_online(name=None, description=None, address=None, color=None, coordinate_x=None,
                           coordinate_y=None, url_site=None, category=None, price=None, user_create_event=None,
                           search_gender=None, photo_event=None, is_online=None, date_event=None, event_online=False):
    if event_online:
        if not Events.objects.filter(description=description, url_site=url_site, price=price, date_event=date_event):
            created_event = Events.objects.create(name=name, description=description, url_site=url_site,
                                                  coordinate_x=0, coordinate_y=0,
                                                  category=category, price=price, create_event=user_create_event,
                                                  photo_event=photo_event, is_online=True, date_event=date_event)
            created_event.save()
            access_on_event = AccessForEvents.objects.create(user=user_create_event,
                                                             event=created_event,
                                                             access=AccessForEvents.CREATED)
            access_on_event.save()
            chat = Chat.objects.create(event=created_event,
                                       photo_chat=photo_event)
            chat.save()
            chat.users.add(user_create_event)

    else:
        if not Events.objects.filter(description=description, url_site=url_site, price=price, date_event=date_event):
            created_event = Events.objects.create(name=name, description=description, address=address,
                                                  url_site=url_site, coordinate_x=coordinate_x,
                                                  coordinate_y=coordinate_y, category=category, price=price,
                                                  create_event=user_create_event, photo_event=photo_event,
                                                  date_event=date_event)
            created_event.save()
            access_on_event = AccessForEvents.objects.create(user=user_create_event,
                                                             event=created_event,
                                                             access=AccessForEvents.CREATED)
            access_on_event.save()
            chat = Chat.objects.create(event=created_event,
                                       photo_chat=photo_event)
            chat.save()
            chat.users.add(user_create_event)
