import re

total = re.compile('[^А-Яа-яA-Za-z0-9/-]')

# Для ФИО, названий городов, улиц
rus = re.compile('[^А-Яа-я\-]')

en = re.compile('[^A-Za-z]')

# Для проверки app_key и хэшей
secret = re.compile('[^A-Za-z0-9]')


number = re.compile('[^0-9]')
login = re.compile(r'[^А-Яа-яA-Za-z0-9\-@\.\_]')

del_html_tags = re.compile('\<[^>]*\>')
