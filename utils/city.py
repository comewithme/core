import logging

from modules.core.models.city import City

LOGGER = logging.getLogger(__name__)


def create_or_update_city():
    citys = [
        {'name': 'Пермь', "latitude": "58.000000", "longitude": "56.316666"},
        {'name': 'Уфа', "latitude": "54.733334", "longitude": "56.000000"},
        {'name': 'Ростов-на-Дону', "latitude": "47.233334", "longitude": "39.700001"},
        {'name': 'Омск', "latitude": "54.983334", "longitude": "73.366669"},
        {'name': 'Екатеринбург', "latitude": "56.833332", "longitude": "60.583332"},
        {'name': 'Санкт-Петербург', "latitude": "59.937500", "longitude": "30.308611"},
        {'name': 'Благовещенск', "latitude": "50.272778", "longitude": "127.540405"},
        {'name': 'Краснодар', "latitude": "45.039268", "longitude": "38.987221"},
        {'name': 'Оренбург', "latitude": "51.776272", "longitude": "55.099594"},
        {'name': 'Самара', "latitude": "53.241505", "longitude": "50.221245"},
        {'name': 'Саратов', "latitude": "51.592365", "longitude": "45.960804"},
        {'name': 'Владимир', "latitude": "56.143063", "longitude": "40.410934"},
        {'name': 'Сочи', "latitude": "43.588348", "longitude": "39.729996"},
        {'name': 'Москва', "latitude": "55.751244", "longitude": "37.618423"},
        {'name': 'Новосибирск', "latitude": "55.018803", "longitude": "82.933952"},
        {'name': 'Красная Поляна', "latitude": "43.6779574", "longitude": "40.2069574"},
        {'name': 'Стерлитамак', "latitude": "53.6019517", "longitude": "55.94395424152057"},
        {'name': 'Казань', "latitude": "55.7893327", "longitude": "49.13883244112836"},
    ]

    for i in citys:
        try:
            City.objects.create(name=i['name'],
                                latitude=i['latitude'],
                                longitude=i['longitude'])
            LOGGER.info(f"[CITY] Create city {i['name']} ")
        except:
            pass


