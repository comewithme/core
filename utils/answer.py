import sys
import json

"""Список возможных ответов клиентов при нештатной ситуации"""

# Пользователь не найден
USER_NOT_FOUND = 'Пользователь не найден'
CHAT_NOT_FOUND = 'Чат не найден'
EVENT_NOT_FOUND = 'События не найдены'
INCORRECT_NEW_PASSWORD = 'Пароль должен быть больше 6 символов'

SEND_EMAIL = 'Письмо отправлено на почту'
SEND_CODE = 'Код отправлен'
SUCCESS_CODE = 'Код валидный'

SUCCESS_ADD_FCM_TOKEN = 'Успешно добавлен FCM токен'
FCM_TOKEN_ALREADY_EXITS = 'Пользователю уже добавлен fcm token'

# Сервис временно недоступен
SERVICE_UNAVAILABLE = 'Сервис временно не доступен'

# Неверный формат запроса или не корректные данные
INCORRECT_DATA = 'Неверный формат запроса или некорректные данные'

# некорректный ввод search_gender
INCORRECT_SEARCH_GENDER = 'Некорректное поле search_gender'

# некорректное поле max_count_users
INCORRECT_MAX_COUNT_USERS = 'Некорректное поле max_count_users'
# некорректное поле birthday
INCORRECT_BIRTHDAY = 'Некорректное поле birthday'

# не корректный ввод tags
INCORRECT_TAGS = 'Некорректное поле tags'

TAGS_NOT_FOUND = 'Теги не найдены'

# некорректный выбор цветового градиента
INCORRECT_COLOR_GRADIENT = 'Некорректное поле цветового градиента'

# нет такого цветового градиента
COLOR_GRADIENT_NOT_FOUND = 'Некорректное поле цветового градиента'

# Неверный пароль
INCORRECT_PASSWORD = 'Указаны неверный логин или пароль'

INCORRECT_PHONE = 'Существует пользователь с указанным телефоном'
INCORRECT_EMAIL = 'Существует пользователь с указанной почтой'

MANY_SEND_SMS = 'Использовано много попыток. Повторите позже'

ERROR_SAVE_IN_CACHE = 'Не удалось сохранить в кэше'

INCORRECT_PASSWORD_REGISTER = 'Указанные пароли не совпадают'

# Отсутствуют обязательные данные в поле data
MISSING_DATA = 'Отсутствуют обязательные данные в поле data'

ERROR_COORDINATES = 'Отсутствуют координаты'

# Нет данных для отправки или пустой ответ
NO_DATA = 'Нет данных для отправки'
NO_ANSWER = 'Пустой ответ'

# Доступ запрещен
ACCESS_DENIED = 'Доступ запрещен'

TOKEN_NOT_VALID = "Токен не валидный"
CODE_NOT_VALID = "Неправильно указан код"
NO_TOKEN = "Не указан токен"

ALREADY_SEND_COMPLAINT = 'Вы уже отправили жалобу'

METHOD_DENIED = 'Метод запрещен'

UNDEF = 'Неизвестная ошибка'

SUCCESS_CREATE_EVENT = 'Событие успешно создано'
SUCCESS_SAVE_IN_CACHE = 'Фото успешно сохранилось в кэше'

SUCCESS_CREATE_USER = 'Успешно создан пользователь'

SUCCESS_CHANGE_USER = 'Данные успешно изменены'
SUCCESS_REMOVE_PHOTO = 'Фотка успешно удалена'
SUCCESS_CHANGE_USER_PASSWORD = 'Пароль успешно изменен'

# Создание жалобы
SUCCESS_CREATE_COMPLAINT = 'Жалоба отправлена'

SUCCESS_DELETE_EVENT = 'Событие успешно удалено'
SUCCESS_ADD_USER_ON_EVENT = 'Успешно добавлен пользователь в событие'
SUCCESS_AWAITING_USER_ON_EVENT = 'Запрос на событие отправлен'
SUCCESS_READ_USER_ON_EVENT = 'Успешно проставлен статус "просмотрено" для пользователя в событии'
SUCCESS_DELETE_USER_FROM_EVENT = 'Успешно удален пользователь из события'

SUCCESS_CHANGE_EVENT = 'Событие успешно изменено'

NOT_FOUND_EVENT = 'Событие не найдено'
SUCCESS_CREATE_FEEDBACK = 'Успешно создан отзыв'

ERROR_CREATE_FEEDBACK = 'Отзыв для данного пользователи и события уже создан'

REQUEST_ACCEPTED = 'Запрос принят'
ERROR_DECODING_SIGNATURE = "Ошибка определения сигнатуры токена"

# Chat
SUCCESS_SEND_MESSAGE = 'Сообщение успешно отправлено'

ANSWER_LIST = {
    SERVICE_UNAVAILABLE: 501,
    INCORRECT_NEW_PASSWORD: 502,
    SUCCESS_CODE: 503,
    INCORRECT_DATA: 505,
    INCORRECT_PASSWORD_REGISTER: 504,
    MISSING_DATA: 506,
    NO_TOKEN: 507,
    NO_DATA: 508,
    ERROR_SAVE_IN_CACHE: 400,
    INCORRECT_PHONE: 509,
    ACCESS_DENIED: 511,
    UNDEF: 522,
    NO_ANSWER: 533,
    METHOD_DENIED: 534,
    USER_NOT_FOUND: 402,
    INCORRECT_PASSWORD: 401,
    ERROR_COORDINATES: 403,
    TOKEN_NOT_VALID: 512,
    SUCCESS_CREATE_EVENT: 201,
    SUCCESS_CREATE_COMPLAINT: 200,
    SUCCESS_DELETE_EVENT: 202,
    SUCCESS_ADD_USER_ON_EVENT: 203,
    SUCCESS_DELETE_USER_FROM_EVENT: 204,
    SUCCESS_CHANGE_EVENT: 205,
    NOT_FOUND_EVENT: 513,

    'Unknown method': 524,
    'Под указанные данные подпадает несколько контрагентов: More than one customer has been found': 525,
    'Request processing failed': 526,
    'Connectivity problem': 527,
    'Internal server error': 528,
    'Invalid request': 529,
    'Unknown server response status': 530,
    'Action isn\u0027t available': 531,
    "Internal server error: Internal server error: На договоре не найден модуль 'Uniteller'": 532,
}

# Для HTTP
ERROR_MESSAGE = json.dumps({"status": 500, "error": "invalid json"})


def print_answer():
    for key, value in ANSWER_LIST.items():
        sys.stdout.write(key, ":", value)
