from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LogoutView
from django.urls import reverse_lazy
from django.conf.urls.static import static

from .settings import local, base

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path(r'logout/', LogoutView.as_view(next_page=reverse_lazy('index')), name='logout'),
                  path('', include('modules.users.urls')),
                  path('api/', include('modules.api.urls'), name='api'),
                  path('admin_panel/', include('modules.admin_panel.urls'), name='api'),
              ] + static(local.MEDIA_URL, document_root=local.MEDIA_ROOT)

if base.DEBUG:
    urlpatterns += static(base.MEDIA_URL, document_root=base.MEDIA_ROOT)
    urlpatterns += static(base.STATIC_URL, document_root=base.STATIC_URL)
