from .base import *

DEBUG = False

MAIN_LDAP_SERVER = ''  # Only for production server

RAVEN_CONFIG = {
    'dsn': '',
}

LOGGING['loggers']['django']['handlers'].append('sentry')
LOGGING['loggers']['django']['level'] = 'INFO'

LOGGING['loggers']['celery']['handlers'].append('sentry')
LOGGING['loggers']['celery']['level'] = 'INFO'

LOGGING['loggers']['modules']['handlers'].append('sentry')
LOGGING['loggers']['modules']['level'] = 'INFO'
