from channels.auth import AuthMiddlewareStack
from channels.db import database_sync_to_async
from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections
from jwt import decode as jwt_decode
from django.conf import settings
from urllib.parse import parse_qs

from modules.users.models import Users


@database_sync_to_async
def get_user(user_id):
    try:
        return Users.objects.get(id=user_id)
    except Users.DoesNotExist:
        return AnonymousUser()


class TokenAuthMiddlewareInstance:
    """
    Custom token auth middleware
    Честная копипаста с интернетов
    """

    def __init__(self, scope, middleware):
        self.middleware = middleware
        self.scope = dict(scope)
        self.inner = self.middleware.inner

    async def __call__(self, receive, send):
        # Try to authenticate the user
        try:
            # Close old database connections to prevent usage of timed out connections
            close_old_connections()

            # Получение токена из квери стринга, можно например через хедер он будет в scope['headers']
            token = parse_qs(self.scope["query_string"].decode("utf8"))["token"][0]

            decoded_token = jwt_decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
            # TODO: Проверка токена на валидность
            user = await get_user(decoded_token['user_id'])
            return await self.inner(dict(self.scope, user=user), receive, send)
        except Exception as e:
            # Token is invalid
            # Тут короче просто в лог написать или например сделать user=AnonymousUser() и там в консьюмере обработать
            print(e)
            return None


class TokenAuthMiddleware:
    """
    Token authorization middleware for Django Channels 2
    see:
    https://channels.readthedocs.io/en/latest/topics/authentication.html#custom-authentication
    """

    def __init__(self, inner):
        self.inner = inner

    def __call__(self, scope):
        return TokenAuthMiddlewareInstance(scope, self)


TokenAuthMiddlewareStack = lambda inner: TokenAuthMiddleware(AuthMiddlewareStack(inner))
